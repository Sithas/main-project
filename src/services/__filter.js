
export const __dashLodashToCamel = (string) => {
  string = string.replace(/[-_\s]+(.)?/g, (match, chr) => {
    return chr ? chr.toUpperCase() : ''
  })
  return string
}

export const __dashToLodash = (string) => {
  string = string.replace(/-/g, '_')
  return string
}

export const __camelToLodash = (string) => {
  string = string.replace(/(?:^|\.?)([A-Z])/g, (x, y) => {
    return '_' + y.toLowerCase()
  }).replace(/^_/, '')
  return string
}

export const __camelToDash = (string) => {
  string = string.replace(/(?:^|\.?)([A-Z])/g, (x, y) => {
    return '-' + y.toLowerCase()
  }).replace(/^-/, '')
  return string
}

export const __formatMoney = (number) => {
  return number.toFixed().toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')
}

export const __cutContent = (string, regExp) => {
  return string.replace(regExp, '')
}
