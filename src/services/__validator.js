export const __validateForm = (form) => {
  const condition = form.value === ''
  switch (form.type) {
    case 'email':
      return condition ? true : validateEmail(form.value)
    case 'phone':
      return condition ? true : validatePhone(form.value)
    case 'luna-card':
      return condition ? true : validateLuhn(form.value.replace(/ /g, ''))
    case 'text':
      return condition ? true : validateText(form.value)
    case 'upperCaseText':
      return condition ? true : validateUpperCaseText(form.value)
    case 'number':
      return condition ? true : validateNumber(form.value)
    default:
      return true
  }
}

const validateEmail = (value) => {
  const pattern = /^([a-z0-9_.-])+@{1}[a-z0-9-.]+\.([a-z]{2,}\.)?[a-z]{2,}$/i
  return pattern.test(value)
}

const validatePhone = (value) => {
  const pattern = /[0-9()+-]/g
  return pattern.test(value) && !/_/g.test(value)
}

const validateText = (value) => {
  const pattern = /[0-9]/g
  return !pattern.test(value)
}

const validateUpperCaseText = (value) => {
  const pattern = /[A-Z]/g
  return pattern.test(value)
}

const validateNumber = (value) => {
  const pattern = /[0-9]/g
  return pattern.test(value)
}

const validateLuhn = (value) => {
  if (value === ' ') return false
  if (/[^0-9-\s]+/.test(value)) return false

  let nCheck = 0
  let bEven = false
  value = value.replace(/\D/g, '')

  for (let n = value.length - 1; n >= 0; n--) {
    let cDigit = value.charAt(n)
    let nDigit = parseInt(cDigit, 10)

    if (bEven) {
      if ((nDigit *= 2) > 9) nDigit -= 9
    }

    nCheck += nDigit
    bEven = !bEven
  }

  return (nCheck % 10) === 0
}
