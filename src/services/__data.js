import { __dashLodashToCamel } from './__filter'

export const __parceObjectKey = (object, key) => {
  const pattern = /[[\]]/g
  if (pattern.test(key)) {
    const keyList = key.replace(/]/g, '').split('[')
    for (let name of keyList) {
      object[name] = {}
      object = object[name]
    }
    return object
  }
}

export const __getObjectKeyFrom = (object, key) => {
  let newObject = object
  const pattern = /[[\]]/g
  if (pattern.test(key)) {
    const keyList = key.replace(/]/g, '').split('[')
    for (let name of keyList) {
      newObject[name] = object[name]
      newObject = newObject[name]
    }
    console.log(newObject)
    return newObject
  }
}

export const __formatObjectPropsToCamel = (object) => {
  for (let prop in object) {
    if (object.hasOwnProperty(prop) && !(prop === __dashLodashToCamel(prop))) {
      object[__dashLodashToCamel(prop)] = object[prop]
      delete object[prop]
    }
  }
  return object
}

export const __formatDictionaryToVivaDropdown = (dictionary) => {
  dictionary = __formatObjectPropsToCamel(dictionary)
  for (let prop in dictionary) {
    if (dictionary.hasOwnProperty(prop)) {
      for (let word of dictionary[prop]) {
        word.text = word.value
        word.value = word.dictionaryId
        word.key = word.dictionaryId
        delete word.dictionaryId
      }
    }
  }
  return dictionary
}

export const __formatCitiesToVivaAutocomplete = (citiesList) => {
  let citiesArray = []
  citiesList = __formatObjectPropsToCamel(citiesList)
  for (let city in citiesList) {
    citiesList[city].key = citiesList[city].name
    citiesList[city].value = citiesList[city].name
    citiesList[city].text = citiesList[city].name
    delete citiesList[city].id
    delete citiesList[city].name
    citiesArray.push(citiesList[city])
  }
  return citiesArray
}

export const __formatCodesToVivaAutocomplete = (codesList) => {
  let codesArray = []
  codesList = __formatObjectPropsToCamel(codesList)
  for (let code in codesList) {
    codesList[code].key = codesList[code].code
    codesList[code].value = codesList[code].code
    codesList[code].text = codesList[code].code
    delete codesList[code].id
    delete codesList[code].code
    codesArray.push(codesList[code])
  }
  return codesArray
}

export const __formatArrayToStringWithSpace = array => array.toString().replace(/,/g, ' ')

export const __calculatePercentByDays = (amount, days, percent) => {
  return (amount * percent / 100) * days + amount
}
