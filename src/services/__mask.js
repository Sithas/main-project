import maskInput from 'vanilla-text-mask'

export const __mask = (maskType) => {
  let myInput = document.querySelector(`div.${maskType} input`)
  switch (maskType) {
    case 'mask-phone':
      maskInput({
        inputElement: myInput,
        mask: ['+', '4', '2', '0', '(', /[1-9]/, /\d/, /\d/, ')', '-', /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]
      })
      break
    case 'mask-card':
      maskInput({
        inputElement: myInput,
        mask: [/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/],
        placeholderChar: '\u2000'
      })
      break
    default:
      return true
  }
}
