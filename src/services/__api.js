import axios from 'axios'
// polyfill for opera-ie-safari!!!
import URLSearchParams from 'url-search-params'
import { __camelToLodash } from './__filter'
import { __formatArrayToStringWithSpace } from './__data'
import { appConfig } from './../configs/config'

const config = {
  withCredentials: true
}

export const __getToken = () => {
  return axios.get(`http://${appConfig.apiUrl}/api/token`, config)
}

export const __recoverPersonData = (token) => {
  return axios.get(`http://${appConfig.apiUrl}/api/getTemporaryUserDataFront/${token.toString()}`, config)
}

export const __getLoanCondition = () => {
  return axios.get(`http://${appConfig.apiUrl}/api/loanCondition/list`, config)
}

export const __createPerson = (personData, cookie) => {
  let params = new URLSearchParams()
  for (let key in personData) {
    if (personData.hasOwnProperty(key) && (key === 'contact[1][value]')) {
      params.append(__camelToLodash(key), personData[key].value.replace(/[+()-]/g, ''))
    } else if (personData.hasOwnProperty(key) && (personData[key].type === 'checkbox')) {
      params.append(__camelToLodash(key), Number(personData[key].value))
    } else if (personData.hasOwnProperty(key) && Boolean(personData[key].value)) {
      params.append(__camelToLodash(key), personData[key].value)
    }
  }

  return axios.post(`http://${appConfig.apiUrl}/api/register/person`, params, config)
}

export const __createCredit = (creditData, cookie) => {
  let params = new URLSearchParams()
  const token = cookie.replace(/token=/g, '')
  const config = {
    withCredentials: true
  }
  params.append(__camelToLodash('person_token'), token)
  for (let key in creditData) {
    // exception - parse cardholder data string into name and surname
    if (creditData.hasOwnProperty(key) && (key === 'requisite[holderNameSurname]')) {
      const arrayOfData = creditData[key].value.split(' ')
      const name = __formatArrayToStringWithSpace(arrayOfData.splice(0, arrayOfData.length - 1))
      const surname = __formatArrayToStringWithSpace(arrayOfData.splice(arrayOfData.length - 1), 1)

      params.append('requisite[holder_name]', name)
      params.append('requisite[holder_surname]', surname)
    } else if (creditData.hasOwnProperty(key) && (key === 'requisite[cardNumber]')) {
      params.append(__camelToLodash(key), creditData[key].value.replace(/ /g, ''))
    } else if (creditData.hasOwnProperty(key) && (key === 'amount')) {
      params.append(__camelToLodash(key), Number(creditData[key].value) * 100)
    } else if (creditData.hasOwnProperty(key) && Boolean(creditData[key].value) && (key !== 'amountDays')) {
      params.append(__camelToLodash(key), creditData[key].value)
    }
  }

  return axios.post(`http://${appConfig.apiUrl}/api/register/credit`, params, config)
}

export const __createInvest = (personData, cookie) => {
  let params = new URLSearchParams()
  for (let key in personData) {
    if (personData.hasOwnProperty(key) && (key === 'contact[1][value]')) {
      params.append(__camelToLodash(key), personData[key].value.replace(/[+()-]/g, ''))
    } else if (personData.hasOwnProperty(key) && (personData[key].type === 'checkbox')) {
      params.append(__camelToLodash(key), Number(personData[key].value))
    } else if (personData.hasOwnProperty(key) && Boolean(personData[key].value)) {
      params.append(__camelToLodash(key), personData[key].value)
    }
  }

  return axios.post(`http://${appConfig.apiUrl}/api/register/invest`, params, config)
}

export const __createInvestCredit = (creditData, cookie) => {
  let params = new URLSearchParams()
  const token = cookie.replace(/token=/g, '')
  const config = {
    withCredentials: true
  }
  params.append(__camelToLodash('person_token'), token)
  for (let key in creditData) {
    // exception - parse cardholder data string into name and surname
    if (creditData.hasOwnProperty(key) && (key === 'requisite[holderNameSurname]')) {
      const arrayOfData = creditData[key].value.split(' ')
      const name = __formatArrayToStringWithSpace(arrayOfData.splice(0, arrayOfData.length - 1))
      const surname = __formatArrayToStringWithSpace(arrayOfData.splice(arrayOfData.length - 1), 1)

      params.append('requisite[holder_name]', name)
      params.append('requisite[holder_surname]', surname)
    } else if (creditData.hasOwnProperty(key) && (key === 'requisite[cardNumber]')) {
      params.append(__camelToLodash(key), creditData[key].value.replace(/ /g, ''))
    } else if (creditData.hasOwnProperty(key) && (key === 'amount')) {
      params.append(__camelToLodash(key), Number(creditData[key].value) * 100)
    } else if (creditData.hasOwnProperty(key) && Boolean(creditData[key].value) && (key !== 'amountDays')) {
      params.append(__camelToLodash(key), creditData[key].value)
    }
  }

  return axios.post(`http://${appConfig.apiUrl}/api/register/investCredit`, params, config)
}

export const __submitCreditAndPerson = (cookie) => {
  let params = new URLSearchParams()
  const token = cookie.replace(/token=/g, '')
  const config = {
    withCredentials: true
  }
  params.append(__camelToLodash('person_token'), token)

  return axios.post(`http://${appConfig.apiUrl}/api/register/submit`, params, config)
}

export const __getDictionaryList = () => {
  const config = {
    withCredentials: true
  }
  return axios.get(`http://${appConfig.apiUrl}/api/dictionary/list`, config)
}

export const __getCitiesListByName = (cityName) => {
  const config = {
    withCredentials: true
  }
  return axios.get(`http://${appConfig.apiUrl}/api/address/citiesAutoComplete/${cityName}`, config)
}

export const __getCodesByValue = (codeValue) => {
  const config = {
    withCredentials: true
  }
  return axios.get(`http://${appConfig.apiUrl}/api/address/codesAutoComplete/${codeValue}`, config)
}
