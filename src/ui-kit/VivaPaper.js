import React, { Component } from 'react'
import Paper from 'material-ui/Paper'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme'

export class VivaPaper extends Component {
  render () {
    const style = {
      width: this.props.width || '100%',
      minHeight: this.props.height || 'auto',
      backgroundImage: `url(${this.props.backgroundImage})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      boxShadow: '0 3px 14px 0 rgba(0,0,0,0.3)',
      borderRadius: '8px',
      marginTop: '24px',
      padding: '20px'
    }
    return (
      <MuiThemeProvider theme={getMuiTheme(darkBaseTheme)}>
        <Paper zDepth={3} style={style}>{this.props.children}</Paper>
      </MuiThemeProvider>
    )
  }
}
