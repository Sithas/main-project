import React, { Component, PropTypes } from 'react'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import SvgIcon from 'material-ui/SvgIcon'

import { componentThemes } from './colors/variables'

const { string, bool } = PropTypes

const styleDefault = {
  cursor: 'pointer',
  width: '70px',
  height: '70px'
}

export class VivaIconLoginTablet extends Component {
  render () {
    const { theme } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={{
          cursor: 'pointer',
          width: '42px',
          height: '42px'
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M0 0h24v24H0z' fill='none' />
          <path d='M10.09 15.59L11.5 17l5-5-5-5-1.41 1.41L12.67 11H3v2h9.67l-2.58 2.59zM19 3H5c-1.11 0-2 .9-2 2v4h2V5h14v14H5v-4H3v4c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconLoginTablet.propTypes = {
  theme: string
}

export class VivaIconLoginMobile extends Component {
  render () {
    const { theme } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={{
          cursor: 'pointer',
          width: '31px',
          height: '31px'
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M0 0h24v24H0z' fill='none' />
          <path d='M10.09 15.59L11.5 17l5-5-5-5-1.41 1.41L12.67 11H3v2h9.67l-2.58 2.59zM19 3H5c-1.11 0-2 .9-2 2v4h2V5h14v14H5v-4H3v4c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconLoginMobile.propTypes = {
  theme: string
}

export class VivaIconCloseMenuTablet extends Component {
  render () {
    const { theme } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={{
          cursor: 'pointer',
          width: '42px',
          height: '42px',
          marginLeft: '24px'
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z' />
          <path d='M0 0h24v24H0z' fill='none' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconCloseMenuTablet.propTypes = {
  theme: string
}

export class VivaIconMenuTablet extends Component {
  render () {
    const { theme } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={{
          cursor: 'pointer',
          width: '42px',
          height: '42px',
          marginLeft: '24px'
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M0 0h24v24H0z' fill='none' />
          <path d='M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconMenuTablet.propTypes = {
  theme: string
}

export class VivaIconCloseMenuMobile extends Component {
  render () {
    const { theme } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={{
          cursor: 'pointer',
          width: '37px',
          height: '37px',
          marginLeft: '23px'
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z' />
          <path d='M0 0h24v24H0z' fill='none' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconCloseMenuMobile.propTypes = {
  theme: string
}

export class VivaIconMenuMobile extends Component {
  render () {
    const { theme } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={{
          cursor: 'pointer',
          width: '37px',
          height: '37px',
          marginLeft: '23px'
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M0 0h24v24H0z' fill='none' />
          <path d='M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconMenuMobile.propTypes = {
  theme: string
}

export class VivaIconLogo extends Component {
  render () {
    const { theme } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={styleDefault} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M0 0h24v24H0z' fill='none' />
          <path d='M19 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-8 12H9.5v-2h-2v2H6V9h1.5v2.5h2V9H11v6zm2-6h4c.55 0 1 .45 1 1v4c0 .55-.45 1-1 1h-4V9zm1.5 4.5h2v-3h-2v3z' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconLogo.propTypes = {
  theme: string
}

export class VivaIconLogoMobile extends Component {
  render () {
    const { theme } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={{
          cursor: 'pointer',
          width: '49px',
          height: '49px'
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M0 0h24v24H0z' fill='none' />
          <path d='M19 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-8 12H9.5v-2h-2v2H6V9h1.5v2.5h2V9H11v6zm2-6h4c.55 0 1 .45 1 1v4c0 .55-.45 1-1 1h-4V9zm1.5 4.5h2v-3h-2v3z' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconLogoMobile.propTypes = {
  theme: string
}

export class VivaIconMaterialNearMe extends Component {
  render () {
    const { theme, constrainToInput } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={{
          cursor: 'pointer',
          width: '28px',
          height: '28px',
          position: constrainToInput ? 'absolute' : 'relative',
          right: constrainToInput ? '0' : null,
          margin: constrainToInput ? '-5px 5px 0px 0px' : null,
          zIndex: 3000
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M0 0h24v24H0V0z' fill='none' />
          <path d='M21 3L3 10.53v.98l6.84 2.65L12.48 21h.98L21 3z' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconMaterialNearMe.propTypes = {
  theme: string,
  constrainToInput: bool
}

export class VivaIconMaterialArrowDown extends Component {
  render () {
    const { theme, constrainToInput } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={{
          cursor: 'pointer',
          width: '28px',
          height: '28px',
          position: constrainToInput ? 'absolute' : 'relative',
          right: constrainToInput ? '0' : null,
          margin: constrainToInput ? '-5px 5px 0px 0px' : null,
          zIndex: 3000
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z' />
          <path d='M0-.75h24v24H0z' fill='none' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconMaterialArrowDown.propTypes = {
  theme: string,
  constrainToInput: bool
}

export class VivaIconMaterialArrowUp extends Component {
  render () {
    const { theme, constrainToInput } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon style={{
          cursor: 'pointer',
          width: '28px',
          height: '28px',
          position: constrainToInput ? 'absolute' : 'relative',
          right: constrainToInput ? '0' : null,
          margin: constrainToInput ? '-5px 5px 0px 0px' : null,
          zIndex: 3000
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path d='M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z' />
          <path d='M0 0h24v24H0z' fill='none' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconMaterialArrowUp.propTypes = {
  theme: string,
  constrainToInput: bool
}

export class VivaIconMaterialPlus extends Component {
  render () {
    const { theme, constrainToInput } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon value={this.props.value} name={this.props.name} onClick={this.props.onClick} style={{
          cursor: 'pointer',
          width: '28px',
          height: '28px',
          position: constrainToInput ? 'absolute' : 'relative',
          right: constrainToInput ? '0' : null,
          margin: constrainToInput ? '-5px 5px 0px 0px' : null,
          zIndex: 3000
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path value={this.props.value} name={this.props.name} d='M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z' />
          <path value={this.props.value} name={this.props.name} d='M0 0h24v24H0z' fill='none' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconMaterialPlus.propTypes = {
  theme: string,
  constrainToInput: bool
}

export class VivaIconMaterialMinus extends Component {
  render () {
    const { theme, constrainToInput } = this.props
    const { $secondaryColor1, $secondaryColor2 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon value={this.props.value} name={this.props.name} onClick={this.props.onClick} style={{
          cursor: 'pointer',
          width: '28px',
          height: '28px',
          position: constrainToInput ? 'absolute' : 'relative',
          right: constrainToInput ? '0' : null,
          margin: constrainToInput ? '-5px 5px 0px 0px' : null,
          zIndex: 3000
        }} color={$secondaryColor1} hoverColor={$secondaryColor2}>
          <path value={this.props.value} name={this.props.name} d='M19 13H5v-2h14v2z' />
          <path value={this.props.value} name={this.props.name} d='M0 0h24v24H0z' fill='none' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconMaterialMinus.propTypes = {
  theme: string,
  constrainToInput: bool
}

export class VivaIconMaterialClose extends Component {
  render () {
    const { theme, constrainToInput } = this.props
    const { $secondaryColor1, $primaryColor1 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon value={this.props.value} name={this.props.name} onClick={this.props.onClick} style={{
          cursor: 'pointer',
          width: '28px',
          height: '28px',
          position: constrainToInput ? 'absolute' : 'relative',
          right: constrainToInput ? '0' : null,
          margin: constrainToInput ? '-5px 5px 0px 0px' : null,
          zIndex: 100
        }} color={$secondaryColor1} hoverColor={$primaryColor1}>
          <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z' />
          <path d='M0 0h24v24H0z' fill='none' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconMaterialClose.propTypes = {
  theme: string,
  constrainToInput: bool
}

export class VivaIconMaterialWarning extends Component {
  render () {
    const { theme, constrainToInput } = this.props
    const { $secondaryColor1, $secondaryColor2, $primaryColor1 } = componentThemes[theme] ? componentThemes[theme] : componentThemes.inputIconsTheme
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <SvgIcon value={this.props.value} name={this.props.name} onClick={this.props.onClick} style={{
          cursor: 'pointer',
          width: '40px',
          height: '40px',
          position: constrainToInput ? 'absolute' : 'relative',
          left: constrainToInput ? '0' : null,
          margin: constrainToInput ? '-10px 0px 0px 0px' : null,
          zIndex: 100
        }} color={$secondaryColor1} hoverColor={$primaryColor1}>
          <path d='M0 0h24v24H0z' fill='none' />
          <path d='M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z' />
        </SvgIcon>
      </MuiThemeProvider>
    )
  }
}

VivaIconMaterialWarning.propTypes = {
  theme: string,
  constrainToInput: bool
}
