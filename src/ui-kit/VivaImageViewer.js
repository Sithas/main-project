import React from 'react'
import { Image } from 'semantic-ui-react'

export const VivaImageViewer = (props) => (
  <Image style={{
    marginTop: '15px'
  }}
    shape='rounded'
    src={props.src}
    as='a' size='big'
    href={props.href}
    target={props.href ? '_blank' : ''}
  />
)
