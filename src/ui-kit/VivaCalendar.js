import React, { Component, PropTypes } from 'react'
import { Button } from 'semantic-ui-react'
import UUID from 'uuid/v4'

import './../styles/workarounds/calendar.scss'

const { func, string, bool } = PropTypes

// full ready settings for default color button
export const defaultButtonFluidProps = {
  basic: true,
  fluid: true,
  color: 'teal',
  size: 'medium'
}

export class VivaCalendar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      key: [UUID(), UUID(), UUID()],
      day: '',
      month: '',
      year: ''
    }
  }
  componentDidMount () {
    // костыли!!! todo: в отдаленной перспективе найти решение без jquery и выпилить КЕМ
    for (let i = 0; i < this.state.key.length; i++) {
      let calendarPart = $(`#example${this.state.key[i]}.viva-calendar`)
      // жесткчайшие костыли - нужны для того чтобы скрывать часть календаря и для выравнивания контента
      // day
      i === 2 && calendarPart.calendar({
        type: 'date',
        popupOptions: {
          position: 'bottom center',
          lastResort: 'bottom center',
          prefer: 'opposite',
          hideOnScroll: false
        },
        disableYear: true,
        onShow: () => {
          document.getElementById(`example${this.state.key[0]}`).getElementsByTagName('thead')[0].style.display = 'none'
        },
        onHide: () => {
          document.getElementById(`example${this.state.key[0]}`).getElementsByTagName('thead')[0].style.display = 'none'
        },
        onChange: (date) => {
          // Еще один костыль - нужна чтобы не сломать обработчик состояния компонента issuePage - handleIssueFormChange
          let value = {
            target: {
              name: this.props.name,
              value: this.props.valueSet === 'simpledate' ? `${this.state.year}-${this.state.month}-${date.toString().split(' ')[2]}` : `${this.state.year}-${this.state.month}${date.toString().split(' ')[2]}`
            }
          }
          this.setState({
            day: value.target.value.split(/-/g)[2]
          })
          this.props.handleParentComponentStateChange(value, value.target.value)
        }
      })
      // month
      i === 1 && calendarPart.calendar({
        type: 'month',
        popupOptions: {
          position: 'bottom center',
          lastResort: 'bottom center',
          prefer: 'opposite',
          hideOnScroll: false
        },
        text: {
          monthsShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
        },
        onShow: () => {
          document.getElementById(`example${this.state.key[1]}`).getElementsByTagName('thead')[0].style.display = 'none'
        },
        onHide: () => {
          document.getElementById(`example${this.state.key[1]}`).getElementsByTagName('thead')[0].style.display = 'none'
        },
        onChange: (date) => {
          let translateMonthToNumber = (month) => {
            switch (month) {
              case 'JAN':
                return '01'
              case 'FEB':
                return '02'

              case 'MAR':
                return '03'
              case 'APR':
                return '04'
              case 'MAY':
                return '05'
              case 'JUN':
                return '06'
              case 'JUL':
                return '07'
              case 'AUG':
                return '08'
              case 'SEP':
                return '09'
              case 'OCT':
                return '10'
              case 'NOV':
                return '11'
              case 'DEC':
                return '12'
              default:
                return true
            }

          }
          // Еще один костыль - нужна чтобы не сломать обработчик состояния компонента issuePage - handleIssueFormChange
          let value = {
            target: {
              name: this.props.name,
              value: this.props.valueSet === 'simpledate' ? `${this.state.year}-${translateMonthToNumber(date.toString().split(' ')[1].toUpperCase())}-${this.state.day}` : `${this.state.year}-${translateMonthToNumber(date.toString().split(' ')[1].toUpperCase())}${this.state.day}`
            }
          }
          this.setState({
            month: value.target.value.split(/-/g)[1]
          })
          this.props.handleParentComponentStateChange(value, value.target.value)
        }
      })
      // year
      i === 0 && calendarPart.calendar({
        type: 'year',
        popupOptions: {
          position: 'bottom center',
          lastResort: 'bottom center',
          prefer: 'opposite',
          hideOnScroll: false
        },
        onChange: (date) => {
          // Еще один костыль - нужна чтобы не сломать обработчик состояния компонента issuePage - handleIssueFormChange
          let value = {
            target: {
              name: this.props.name,
              value: this.props.valueSet === 'simpledate' ? `${date.toString().split(' ')[3]}-${this.state.month}-${this.state.day}` : `${date.toString().split(' ')[3]}-${this.state.month}${this.state.day}`
            }
          }
          this.setState({
            year: value.target.value.split(/-/g)[0]
          })
          this.props.handleParentComponentStateChange(value, value.target.value)
        }
      })
    }
  }
  render () {
    const {fluid} = this.props
    let filterContent = (content) => {
      if (typeof content === 'string') {
        return content.toUpperCase()
      } else {
        return content
      }
    }

    return (
      <div>
        {this.props.valueSet === 'simpledate' ? (<Button.Group fluid={fluid} name={this.props.name} value={this.props.value}>
          <div className='ui teal medium basic button calendar viva-calendar' id={`example${this.state.key[2]}`}>
            <div className='ui calendar-button'>{filterContent(this.state.day) || this.props.value.split('-')[2] || 'ДЕНЬ'}</div>
          </div>
          <div className='ui teal medium basic button calendar viva-calendar' id={`example${this.state.key[1]}`}>
            <div className='ui calendar-button'>{filterContent(this.state.month) || this.props.value.split('-')[1] || 'МЕСЯЦ'}</div>
          </div>
          { this.props.valueSet === 'simpledate' && (<div className='ui teal medium basic button calendar viva-calendar' id={`example${this.state.key[0]}`}>
            <div className='ui calendar-button'>{filterContent(this.state.year) || this.props.value.split('-')[0] || 'ГОД'}</div>
          </div>)}
        </Button.Group>)
          : (
            <Button.Group fluid={fluid} name={this.props.name} value={this.props.value}>
              <div className='ui teal medium basic button calendar viva-calendar' id={`example${this.state.key[1]}`}>
                <div className='ui calendar-button'>{filterContent(this.state.month) || this.props.value.split('-')[1] || 'МЕСЯЦ'}</div>
              </div>
              <div className='ui teal medium basic button calendar viva-calendar' id={`example${this.state.key[0]}`}>
                <div className='ui calendar-button'>{filterContent(this.state.year) || this.props.value.split('-')[2] || 'ГОД'}</div>
              </div>
            </Button.Group>
          )}
      </div>
    )
  }
}

VivaCalendar.propTypes = {
  handleParentComponentStateChange: func,
  fluid: bool,
  value: string,
  valueSet: string,
  name: string
}
