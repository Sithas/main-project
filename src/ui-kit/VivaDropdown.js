import React, { Component, PropTypes } from 'react'
import { Dropdown } from 'semantic-ui-react'
import { VivaIconMaterialNearMe, VivaIconMaterialArrowDown } from './VivaIcons'
// imports custom style
import './../styles/workarounds/dropdowns.scss'

const { string, bool, func, any, number } = PropTypes

export const defaultDropdownFluidProps = {
  fluid: true,
  icon: 'ArrowDrop'
}

export const defaultAutocompleteFluidProps = {
  fluid: true,
  search: true,
  icon: 'Finder'
}

export class VivaDropdown extends Component {
  constructor (props) {
    super(props)
    this.state = {
      value: 'ru'
    }
  }

  render () {
    const { placeholder, loading, error, disabled, fluid, search, icon, name, value } = this.props
    const props = { placeholder, loading, error, disabled, fluid, search, name, value }
    const iconList = {
      Finder: <VivaIconMaterialNearMe constrainToInput theme={'inputIconsTheme'} />,
      ArrowDrop: <VivaIconMaterialArrowDown constrainToInput theme={'inputIconsTheme'} />
    }
    return (
      <div>
        <Dropdown icon={iconList[icon]} size='big'
          onChange={this.props.handleParentComponentStateChange}
          onSearchChange={this.props.handleDrawContent}
          {...props}
          selection options={this.props.options}
          value={this.props.value} />
      </div>
    )
  }
}

VivaDropdown.propTypes = {
  placeholder: string,
  loading: bool,
  name: string,
  value: any,
  error: bool,
  disabled: bool,
  fluid: bool,
  search: bool,
  icon: string,
  handleParentComponentStateChange: func,
  handleDrawContent: func,
  options: any
}
