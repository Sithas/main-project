import React, { Component } from 'react'
import Slider from 'material-ui/Slider'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { componentThemes } from './colors/variables'

const { $primaryColor1, $secondaryColor1, $alterColor1, $borderColor1, $textColor1 } = componentThemes.defaultTheme

const muiTheme = getMuiTheme({
  slider: {
    trackColor: $borderColor1,
    trackColorSelected: $textColor1,
    handleFillColor: $secondaryColor1,
    handleColorZero: $alterColor1,
    handleSize: 18,
    selectionColor: $primaryColor1,
    trackSize: 1
  }
})

export class VivaSlider extends Component {
  render () {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <Slider
          name={this.props.name}
          style={{margin: '0 0', zIndex: '1000'}}
          min={this.props.min}
          max={this.props.max}
          step={this.props.step}
          value={this.props.value}
          onChange={(event, value) => {
            const context = this
            return context.props.handleSlider({
              target: {
                name: context.props.name,
                value: value
              }
            })
          }}
        />
      </MuiThemeProvider>
    )
  }
}
