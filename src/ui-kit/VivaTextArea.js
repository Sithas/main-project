import React, { Component, PropTypes } from 'react'
import { TextArea } from 'semantic-ui-react'
import './../styles/workarounds/textarea.scss'

const { string, bool, func } = PropTypes

export const defaultTextAreaFluidProps = {
  fluid: true
}

export class VivaTextArea extends Component {
  render () {
    const { placeholder, loading, disabled, fluid, value, name } = this.props
    const props = { placeholder, loading, disabled, fluid }
    const inputStyle = {
      marginBottom: '30px',
      display: 'block',
      padding: '6px 12px',
      fontSize: '15px',
      width: '100%',
      color: 'gray',
      backgroundColor: '#fff',
      backgroundImage: 'none',
      border: '1px solid #ccc',
      borderRadius: '5px',
      boxShadow: 'none',
      outline: '0'
    }
    return (
      <div>
        <TextArea style={inputStyle} name={name} onInput={this.props.handleParentComponentStateChange} {...props} value={value} />
      </div>
    )
  }
}

VivaTextArea.propTypes = {
  placeholder: string,
  loading: bool,
  disabled: bool,
  fluid: bool,
  value: string,
  handleParentComponentStateChange: func,
  name: string
}
