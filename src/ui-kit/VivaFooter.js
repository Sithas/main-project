import React, { Component } from 'react'

export class VivaFooter extends Component {
  render () {
    return (
      <div className='col-xs-12'>
        <footer id='footer'>
          <div className='footer-top-container'>
            <div className='row'>
              <div className='col-sm-4 col-md-3 mb-30px'>
                <div className='footer-title mb-25px'>
                  информация
                </div>
                {/* footer-title */}
                <div className='footer-menu'>
                  <ul className='list-unstyled'>
                    <li><a href='#'>Как взять кредит</a></li>
                    <li><a href='#'>Как погасить кредит</a></li>
                    <li><a href='#'>Вопросы и ответы</a></li>
                    <li><a href='#'>Привелегии</a></li>
                  </ul>
                  {/* list-unstyled */}
                </div>
                {/* footer-menu */}
              </div>
              {/* col-sm-4 */}
              <div className='col-sm-4 col-md-3 mb-30px'>
                <div className='footer-title mb-25px'>
                  компания
                </div>
                {/* footer-title */}
                <div className='footer-menu'>
                  <ul className='list-unstyled'>
                    <li><a href='#'>О нас</a></li>
                    <li><a href='#'>Акции и новости</a></li>
                    <li><a href='#'>Сми о нас</a></li>
                    <li><a href='#'>Партнерам</a></li>
                  </ul>
                  {/* list-unstyled */}
                </div>
                {/* footer-menu */}
              </div>
              {/* col-sm-4 */}
              <div className='col-sm-4 col-md-3 mb-30px'>
                <div className='footer-title mb-25px'>
                  профиль
                </div>
                {/* footer-title */}
                <div className='footer-menu'>
                  <ul className='list-unstyled'>
                    <li><a href='#'>Мой кабинет</a></li>
                    <li><a href='#'>Вход для партнеров</a></li>
                  </ul>
                  {/* list-unstyled */}
                </div>
                {/* footer-menu */}
              </div>
              {/* col-sm-4 */}
              <div className='col-sm-12 col-md-3 clearfix'>
                <div className='hidden-sm'>
                  <div className='footer-title mb-25px '>
                    Поддержка
                  </div>
                  {/* footer-title */}
                  <a href='tel:+74996700760' className='footer-phone'>+7 (499) 670-07-60</a>
                  <div className='footer-menu'>
                    <ul className='list-unstyled'>
                      <li><a href='#'>Обратная связь</a></li>
                    </ul>
                    {/* list-unstyled */}
                  </div>
                  {/* footer-menu */}
                  <a className='footer-mail' href='mailto:info@4slovo.ru'>info@4slovo.ru</a>
                </div>
                {/* hidden-sm */}
                <div className='visible-sm f-bot'>
                  <div className='row'>
                    <div className='col-sm-8'>
                      <div className='footer-title mb-25px '>
                        Поддержка
                      </div>
                      {/* footer-title */}
                      <a href='tel:+74996700760' className='footer-phone'>+7 (499) 670-07-60</a>
                    </div>
                    {/* col-sm-8 */}
                    <div className='col-sm-4'>
                      <div className='footer-menu'>
                        <ul className='list-unstyled'>
                          <li><a href='#'>Обратная связь</a></li>
                        </ul>
                        {/* list-unstyled */}
                      </div>
                      {/* footer-menu */}
                      <a className='footer-mail' href='mailto:info@4slovo.ru'>info@4slovo.ru</a>
                    </div>
                    {/* col-sm-4 */}
                  </div>
                  {/* row */}
                </div>
                {/* visible-sm */}
              </div>
              {/* col-sm-4 */}
            </div>
            {/* row */}
          </div>
          {/* footer-top-container */}
          <div className='text-center'>
            <a href='/' className='logo-footer mb-30px'><img src='images/logo-footer.png' alt/></a>
            <div className='copyright'>© 2013–2016 ООО МФО «Честное слово». Все права защищены.</div>
            <div className='footer-socials'>
              <a href='#' target='_blank'><img src='images/vk.png' alt/></a>
              <a href='#' target='_blank'><img src='images/facebook.png' alt/></a>
              <a href='#' target='_blank'><img src='images/ok.png' alt/></a>
            </div>
          </div>
        </footer>
      </div>
    )
  }
}
