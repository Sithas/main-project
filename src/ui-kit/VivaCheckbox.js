import React, { Component, PropTypes } from 'react'
import Checkbox from 'material-ui/Checkbox'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { componentThemes } from './colors/variables'

const { string, func, any } = PropTypes

const { $primaryColor1, $textColor1 } = componentThemes.defaultTheme

const styles = {
  checkbox: {
    marginBottom: 16,
    width: 'auto',
    borderRadius: '5px',
    display: 'inline-block'
  }
}

const muiTheme = getMuiTheme({
  checkbox: {
    boxColor: $textColor1,
    checkedColor: $primaryColor1,
    disabledColor: 'rgba(0, 0, 0, 0.3)',
    labelColor: $textColor1,
    labelDisabledColor: 'rgba(0, 0, 0, 0.3)',
    requiredColor: $primaryColor1
  }
})

export class VivaCheckbox extends Component {
  render () {
    const { name, checked } = this.props
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <Checkbox
          value='checkbox'
          name={name}
          onCheck={this.props.handleParentComponentStateChange}
          style={styles.checkbox}
        />
      </MuiThemeProvider>
    )
  }
}

VivaCheckbox.propTypes = {
  handleParentComponentStateChange: func,
  name: string,
  checked: any
}
