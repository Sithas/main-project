import React, { Component } from 'react'
import Paper from 'material-ui/Paper'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme'
import { componentThemes } from './colors/variables'
import { VivaIconMaterialClose } from './VivaIcons'

const { defaultTheme } = componentThemes
const { $errorColor, $primaryColor1, $secondaryColor1 } = defaultTheme

export const defaultErrorProps = {
  background: $errorColor
}

export const defaultSuccessProps = {
  background: $primaryColor1,
  color: $secondaryColor1
}

export class VivaPaperMessage extends Component {
  render () {
    const style = {
      width: this.props.width || '100%',
      minHeight: this.props.height || 'auto',
      background: `${this.props.background}`,
      boxShadow: `0 3px 14px 0 ${this.props.background}`,
      borderRadius: '8px',
      marginTop: '24px',
      padding: '20px'
    }
    return (
      <MuiThemeProvider theme={getMuiTheme(darkBaseTheme)}>
        <Paper zDepth={3} style={style}>
          <div className='row'>
            <div className='col-xs-12'>
              <div className='col-xs-1'>
                {this.props.leftIcon}
              </div>
              <div className='pull-left'>
                <h4 style={{color: this.props.color}}>{this.props.children}</h4>
              </div>
              <div className='pull-right'>
                <VivaIconMaterialClose theme={this.props.theme} constrainToInput />
              </div>
            </div>
          </div></Paper>
      </MuiThemeProvider>
    )
  }
}
