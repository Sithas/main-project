import React, { Component } from 'react'
import { VivaIconMaterialMinus, VivaIconMaterialPlus } from './VivaIcons'
import { VivaSlider } from './VivaSlider'
import { componentThemes } from './colors/variables'

const { $borderColor1 } = componentThemes.defaultTheme

export class VivaSliderInput extends Component {
  render () {
    const innerMargin = {
      margin: '0 12px'
    }
    const transparentInput = {
      outline: 0,
      border: 0,
      fontSize: '24px',
      fontWeight: '300',
      marginTop: '-50px',
      textAlign: 'center',
      width: 'calc(100% - 56px)',
      transform: 'translateY(-5px)',
      background: 'transparent',
      color: this.props.color
    }
    const numberBlock = {
      transform: 'translateY(30px)'
    }
    const sliderBorder = {
      position: 'absolute',
      border: `1px solid ${$borderColor1}`,
      // borderBottom: 0,
      borderRadius: '8px',
      minHeight: '55px',
      width: 'calc(100% - 54px)',
      transform: 'translateY(-21px)',
      pointerEvents: 'none'
    }
    const sliderWidth = {
      margin: '0 8px 8px'
    }
    return (
      <div style={innerMargin}>
        <div style={numberBlock}>
          <VivaIconMaterialMinus name={this.props.name} onClick={() => {
            const context = this
            return context.props.handleParentComponentStateChange({
              target: {
                name: context.props.name,
                value: this.props.min >= Number(this.props.value) ? Number(this.props.min) : Number(this.props.value) - Number(this.props.step)
              }
            })
          }} theme={'inputIconsTheme'} />
          <input name={this.props.name} readOnly value={this.props.value} onChange={this.props.handleParentComponentStateChange} style={transparentInput} />
          <VivaIconMaterialPlus name={this.props.name} onClick={() => {
            const context = this
            return context.props.handleParentComponentStateChange({
              target: {
                name: context.props.name,
                value: this.props.max <= Number(this.props.value) ? Number(this.props.max) : Number(this.props.value) + Number(this.props.step)
              }
            })
          }} theme={'inputIconsTheme'} />
        </div>
        <div style={sliderBorder} className='slider-border' />
        <div style={sliderWidth}>
          <VivaSlider name={this.props.name}
            value={this.props.value}
            handleSlider={this.props.handleParentComponentStateChange}
            min={this.props.min}
            max={this.props.max}
            step={this.props.step}
          />
        </div>
      </div>
    )
  }
}
