import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { VivaHeaderDrawerMobile } from './VivaHeaderDrawerMobile'
import { VivaIconLoginMobile, VivaIconLogoMobile } from './../ui-kit/VivaIcons'

import { componentThemes } from './colors/variables'

const { string } = PropTypes
const $minHeight = '76px'

export class VivaHeaderMobile extends Component {
  constructor (props) {
    super(props)
    this.state = {
      value: 3
    }
  }

  handleChange (event) {
    const value = event.target.value
    this.setState({value})
  }

  render () {
    const { theme } = this.props
    const { $primaryColor1, $secondaryColor1 } = componentThemes[theme]
    let styleMainBlock = {
      background: $primaryColor1,
      color: $secondaryColor1,
      minHeight: $minHeight,
      boxShadow: theme !== 'invertedTheme' ? '0 3px 17px 0 rgba(0,0,0,0.6)' : 'none'
    }
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <Toolbar style={styleMainBlock}>
          <ToolbarGroup firstChild>
            <VivaHeaderDrawerMobile theme={theme} />
          </ToolbarGroup>
          <ToolbarGroup>
            <Link to='/'><VivaIconLogoMobile theme={theme} /></Link>
          </ToolbarGroup>
          <ToolbarGroup>
            <VivaIconLoginMobile theme={theme} />
          </ToolbarGroup>
        </Toolbar>
      </MuiThemeProvider>
    )
  }
}

VivaHeaderMobile.propTypes = {
  theme: string
}
