import React, { Component, PropTypes } from 'react'
import MediaQuery from 'react-responsive'
import { VivaHeaderDesktop } from './VivaHeaderDesktop'
import { VivaHeaderTablet } from './VivaHeaderTablet'
import { VivaHeaderMobile } from './VivaHeaderMobile'
import injectTapEventPlugin from 'react-tap-event-plugin'

const { string } = PropTypes

// activate tap event for material ui
injectTapEventPlugin()

export class VivaHeader extends Component {
  render () {
    /**
     * theme - constant move to lower components
     * it send themes from ui-kit/colors/variables
     */
    const { theme } = this.props
    return (
      <div>
        <MediaQuery minWidth={1024}>
          <VivaHeaderDesktop theme={theme} />
        </MediaQuery>

        <MediaQuery minWidth={700} maxWidth={1023}>
          <VivaHeaderTablet theme={theme} />
        </MediaQuery>

        <MediaQuery minWidth={300} maxWidth={699}>
          <VivaHeaderMobile theme={theme} />
        </MediaQuery>
      </div>
    )
  }
}

VivaHeader.propTypes = {
  theme: string
}
