import React, { Component, PropTypes } from 'react'
import { VivaButton, defaultButtonFluidProps } from './VivaButton'

export class VivaFileUploader extends Component {
  render () {
    const style = {
      position: 'absolute',
      opacity: 0,
      cursor: 'pointer',
      width: '100%',
      minHeight: '40px',
      marginTop: '-12px',
      marginLeft: '-36px'
    }
    return (
      <VivaButton {...defaultButtonFluidProps}
        value={<span><input name={this.props.name} style={style} onChange={this.props.handleParentComponentStateChange} type='file' id='imgInp' />ЗАГРУЗИТЬ ИЗОБРАЖЕНИЕ</span>} />
    )
  }
}
