import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router-dom'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import { VivaIconMenuTablet, VivaIconLoginTablet, VivaIconLogo, VivaIconCloseMenuTablet } from './VivaIcons'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

const { string } = PropTypes

export class VivaHeaderDrawerTablet extends Component {
  constructor (props) {
    super(props)
    this.state = {open: false}

    this.handleToggle = this.handleToggle.bind(this)
  }

  handleToggle () {
    this.setState({open: !this.state.open})
  }
  render () {
    const { theme } = this.props
    const menuStyle = {
      backgroundColor: 'rgba(0, 0, 0, 0.85)'
    }
    const whiteText = {
      fontSize: '18px',
      marginBottom: '25px',
      marginLeft: '25px',
      marginTop: '25px'
    }
    const styleMainBlock = {
      backgroundColor: 'transparent',
      marginTop: '15px'
    }

    return (
      <div>
        <a onClick={this.handleToggle}><VivaIconMenuTablet theme={theme} /></a>
        <Drawer containerStyle={menuStyle} width='100%' docked={false} open={this.state.open}>
          <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
            <Toolbar style={styleMainBlock}>
              <ToolbarGroup firstChild>
                <Link onClick={this.handleToggle} to='#'><VivaIconCloseMenuTablet theme={theme} /></Link>
              </ToolbarGroup>
              <ToolbarGroup>
                <Link to='/'><VivaIconLogo theme={theme} /></Link>
              </ToolbarGroup>
              <ToolbarGroup>
                <VivaIconLoginTablet theme={theme} />
              </ToolbarGroup>
            </Toolbar>
          </MuiThemeProvider>
          <MenuItem style={whiteText} onClick={this.handleToggle}><Link style={whiteText} className={'header-link__inverted'} to='#'>{'О нас'.toUpperCase()}</Link></MenuItem>
          <MenuItem style={whiteText} onClick={this.handleToggle}><Link style={whiteText} className={'header-link__inverted'} to='#'>{'Как взять кредит'.toUpperCase()}</Link></MenuItem>
          <MenuItem style={whiteText} onClick={this.handleToggle}><Link style={whiteText} className={'header-link__inverted'} to='#'>{'Как погасить кредит'.toUpperCase()}</Link></MenuItem>
          <MenuItem style={whiteText} onClick={this.handleToggle}><Link style={whiteText} className={'header-link__inverted'} to='/questions'>{'Вопросы и ответы'.toUpperCase()}</Link></MenuItem>
          <MenuItem style={whiteText} onClick={this.handleToggle}><Link style={whiteText} className={'header-link__inverted'} to='#'>{'Привилегии'.toUpperCase()}</Link></MenuItem>
          <MenuItem style={whiteText} onClick={this.handleToggle}><Link style={whiteText} className={'header-link__inverted'} to='/invest'>{'Регистрация инвестора'.toUpperCase()}</Link></MenuItem>
        </Drawer>
      </div>
    )
  }
}

VivaHeaderDrawerTablet.propTypes = {
  theme: string
}
