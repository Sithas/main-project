import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router-dom'
import { __cutContent, __camelToDash } from './../services/__filter'

import './../styles/workarounds/links.scss'

const { string } = PropTypes

export class VivaHeaderDesktopTab extends Component {
  render () {
    const { href, theme, value } = this.props
    const themeName = __camelToDash(__cutContent(theme, /Theme/g))
    return (
      <Link to={href} className={'header-link__' + themeName}>{value.toUpperCase()}</Link>
    )
  }
}

VivaHeaderDesktopTab.propTypes = {
  href: string,
  theme: string,
  value: string
}
