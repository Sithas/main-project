import React, { Component, PropTypes } from 'react'
import { Step, Stepper, StepLabel, StepContent } from 'material-ui/Stepper'
import { VivaButton, invertedButtonFluidProps, alternateButtonFluidProps } from './VivaButton'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { componentThemes } from './colors/variables'
import ExpandTransition from 'material-ui/internal/ExpandTransition';

const {any} = PropTypes

const { $primaryColor1, $textColor1, $borderColor1 } = componentThemes.defaultTheme

const muiTheme = getMuiTheme({
  stepper: {
    backgroundColor: 'transparent',
    connectorLineColor: $borderColor1,
    disabledTextColor: $textColor1,
    hoverBackgroundColor: 'rgba(0, 0, 0, 0.06)',
    hoveredIconColor: $primaryColor1,
    iconColor: $primaryColor1,
    inactiveIconColor: $textColor1,
    textColor: $primaryColor1
  }
})

export class VivaStepper extends Component {
  constructor () {
    super()
    this.state = {
      loading: false,
      finished: false,
      stepIndex: 0
    }
    this.handleNext = this.handleNext.bind(this)
    this.handlePrev = this.handlePrev.bind(this)
    this.renderStepActions = this.renderStepActions.bind(this)
  }

  dummyAsync = (cb) => {
    this.setState({loading: true}, () => {
      this.asyncTimer = setTimeout(cb, 500)
    })
  }

  handleNext () {
    const {stepIndex} = this.state
    if (!this.state.loading) {
      this.dummyAsync(() => this.setState({
        loading: false,
        stepIndex: stepIndex + 1,
        finished: stepIndex >= 4,
      }))
    }
  }

  handlePrev ()  {
    const { stepIndex } = this.state
      if (!this.state.loading) {
        this.dummyAsync(() => this.setState({
          loading: false,
          stepIndex: stepIndex - 1,
        }))
      }
  }

  renderStepActions (step) {
    return (
      <div className='row' style={{margin: '12px 0'}}>
        <div className='col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3'>
          {step < this.props.content.length && (
            <VivaButton {...invertedButtonFluidProps} handleParentComponentStateChange={this.handleNext} value='Продолжить' />
          )}
          {step === this.props.content.length && (
            <VivaButton handleParentComponentStateChange={this.props.handleSubmitForm} disabled={!this.props.isFinished} {...invertedButtonFluidProps} value='Завершить' />
          )}
          {step > 1 && (
            <VivaButton {...alternateButtonFluidProps} handleParentComponentStateChange={this.handlePrev} value='Вернуться к предыдущему шагу' />
          )}
        </div>
      </div>
    )
  }

  render () {
    const { stepIndex, loading } = this.state
    const { content } = this.props
    let key = 0

    return (
      <MuiThemeProvider muiTheme={getMuiTheme(muiTheme)}>
        <div style={{width: '100%'}}>
          <div className='col-md-12'>
            <div className="row">
              <div className="col-sm-6 col-sm-offset-3">
                <Stepper activeStep={stepIndex} orientation='horizontal'>
                  {content.map(contentStep => (
                    <Step key={key++}>
                      <StepLabel>{contentStep.label}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
              </div>
            </div>
            <ExpandTransition loading = {loading} open={true}>
              {content[this.state.stepIndex].content}
              {this.renderStepActions(this.state.stepIndex+1)}
            </ExpandTransition>
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

VivaStepper.propTypes = {
  content: any
}
