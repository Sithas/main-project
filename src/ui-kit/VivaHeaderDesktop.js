import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import { VivaHeaderDesktopTab } from './VivaHeaderDesktopTab'
import { VivaButton, alternateButtonProps, defaultButtonProps } from './VivaButton'
import { VivaIconLogo } from './../ui-kit/VivaIcons'

import { componentThemes } from './colors/variables'

const { string } = PropTypes
const $minHeight = '85px'

export class VivaHeaderDesktop extends Component {
  render () {
    const { theme } = this.props
    const { $primaryColor1, $secondaryColor1 } = componentThemes[theme]
    let styleMainBlock = {
      background: $primaryColor1,
      color: $secondaryColor1,
      minHeight: $minHeight,
      boxShadow: theme !== 'invertedTheme' ? '0 3px 17px 0 rgba(0,0,0,0.6)' : 'none'
    }
    let loginButton = () => {
      if (theme === 'defaultTheme') {
        return <VivaButton {...alternateButtonProps} value={<Link to='/login'>ВОЙТИ</Link>} />
      } else {
        return <VivaButton {...defaultButtonProps} value={<Link to='/login'>ВОЙТИ</Link>} />
      }
    }
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        <Toolbar style={styleMainBlock}>
          <ToolbarGroup firstChild>
            <Link to='/'><VivaIconLogo theme={theme} /></Link>
            <VivaHeaderDesktopTab theme={theme} href={'/routeWorks'} value={'О нас'} />
            <VivaHeaderDesktopTab theme={theme} href={'/issue'} value={'Как взять кредит'} />
            <VivaHeaderDesktopTab theme={theme} href={'/routeWorks'} value={'Как погасить кредит'} />
            <VivaHeaderDesktopTab theme={theme} href={'/questions'} value={'Вопросы и ответы'} />
            <VivaHeaderDesktopTab theme={theme} href={'/routeWorks'} value={'Привилегии'} />
            <VivaHeaderDesktopTab theme={theme} href={'/invest'} value={'Регистрация инвестора'} />
          </ToolbarGroup>
          <ToolbarGroup>
            {loginButton()}
          </ToolbarGroup>
        </Toolbar>
      </MuiThemeProvider>
    )
  }
}

VivaHeaderDesktop.propTypes = {
  theme: string
}
