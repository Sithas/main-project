import React, { Component, PropTypes } from 'react'
import { Button } from 'semantic-ui-react'

import './../styles/workarounds/buttons.scss'

const { string, bool, func, obj } = PropTypes

// full ready settings for default color button
export const defaultButtonFluidProps = {
  basic: true,
  fluid: true,
  color: 'teal',
  size: 'medium'
}
export const defaultButtonProps = {
  basic: true,
  color: 'teal',
  size: 'medium'
}
// default style for white background
export const extendedButtonFluidProps = {
  basic: true,
  fluid: true,
  color: 'grey',
  size: 'medium'
}

// alternate button style
export const alternateButtonProps = {
  white: true,
  size: 'medium',
  color: 'white'
}

export const alternateButtonFluidProps = {
  basic: true,
  fluid: true,
  size: 'medium'
}

// inverted button
export const invertedButtonFluidProps = {
  fluid: true,
  color: 'teal',
  size: 'medium'
}

export const tealButtonFluidProps = {
  fluid: true,
  color: 'dark',
  size: 'medium'
}

export const invertedButtonProps = {
  color: 'teal',
  size: 'medium'
}

export class VivaButton extends Component {
  render () {
    const { basic, color, disabled, fluid, loading, value, size } = this.props
    const props = { basic, color, disabled, fluid, loading, size }

    return (
      <Button {...props} onClick={this.props.handleParentComponentStateChange}>{(typeof value === 'string' && value.toUpperCase()) || value}</Button>
    )
  }
}

VivaButton.propTypes = {
  color: string,
  value: string || obj,
  loading: bool,
  disabled: bool,
  fluid: bool,
  basic: bool,
  size: string,
  handleParentComponentStateChange: func
}
