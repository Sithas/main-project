const $teal = '#4ecbc4'
const $white = '#ffffff'
const $purple = '#2a6496'
const $darkGrey = '#333'
const $lightgrey = '#cccccc'
const $red = '#ff7485'

export const $transparent = 'transparent'

export const $standartFontSize = '14px'

export const componentThemes = {
  defaultTheme: {
    $primaryColor1: $teal,
    $primaryColor2: $white,
    $secondaryColor1: $white,
    $secondaryColor2: $darkGrey,
    $alterColor1: $red,
    $textColor1: $darkGrey,
    $borderColor1: $lightgrey,
    $errorColor: $red
  },
  invertedTheme: {
    $primaryColor1: $transparent,
    $primaryColor2: $white,
    $secondaryColor1: $white,
    $secondaryColor2: $teal,
    $alterColor1: $purple,
    $textColor1: $teal,
    $borderColor1: $lightgrey,
    $errorColor: $red
  },
  invertedBackgroundTheme: {
    $primaryColor1: $darkGrey,
    $primaryColor2: $white,
    $secondaryColor1: $white,
    $secondaryColor2: $teal,
    $alterColor1: $purple,
    $textColor1: $teal,
    $borderColor1: $lightgrey,
    $errorColor: $red
  },
  inputIconsTheme: {
    $primaryColor1: $transparent,
    $primaryColor2: $transparent,
    $secondaryColor1: $teal,
    $secondaryColor2: $purple,
    $alterColor1: $transparent,
    $textColor1: $transparent,
    $borderColor1: $transparent,
    $errorColor: $red
  },
  warningIconsTheme: {
    $primaryColor1: $teal,
    $primaryColor2: $transparent,
    $secondaryColor1: $white,
    $secondaryColor2: $purple,
    $alterColor1: $transparent,
    $textColor1: $transparent,
    $borderColor1: $transparent,
    $errorColor: $red
  },

}
