import React, { Component, PropTypes } from 'react'
import { Button } from 'semantic-ui-react'

import './../styles/workarounds/buttongroups.scss'

const {string, bool, arrayOf, any, func} = PropTypes

// full ready settings for default color button
export const defaultButtonFluidProps = {
  basic: true,
  fluid: true,
  color: 'teal',
  size: 'medium'
}
export const defaultButtonProps = {
  basic: true,
  color: 'teal',
  size: 'medium'
}
// default style for white background
export const extendedButtonFluidProps = {
  basic: true,
  fluid: true,
  color: 'grey',
  size: 'medium'
}

// alternate button style
export const alternateButtonProps = {
  basic: true,
  size: 'medium'
}

export const alternateButtonFluidProps = {
  basic: true,
  fluid: true,
  size: 'medium'
}

// inverted button
export const invertedButtonFluidProps = {
  fluid: true,
  color: 'teal',
  size: 'medium'
}
export const invertedButtonProps = {
  color: 'teal',
  size: 'medium'
}

export class VivaButtonGroup extends Component {
  render () {
    const {basic, color, disabled, fluid, loading, valueSet, size, activeCondition} = this.props
    const props = {basic, color, disabled, loading, size}
    let filterContent = (content) => {
      if (typeof content === 'string') {
        return content.toUpperCase()
      } else {
        return content
      }
    }
    let key = 1
    return (
      <Button.Group fluid={fluid} value={this.props.value}>
        {valueSet.map((value) => {
          return <Button active={value.model === activeCondition} name={this.props.name} key={key++} {...props} onClick={this.props.handleParentComponentStateChange} value={value.model}>{filterContent(value.view)}</Button>
        })}
      </Button.Group>
    )
  }
}

VivaButtonGroup.propTypes = {
  color: string,
  valueSet: arrayOf(any),
  loading: bool,
  disabled: bool,
  fluid: bool,
  basic: bool,
  size: string,
  activeCondition: any,
  name: string,
  value: any,
  handleParentComponentStateChange: func
}
