import React, { Component, PropTypes } from 'react'
import { Input } from 'semantic-ui-react'
import { __mask } from './../services/__mask'
// imports custom style
import './../styles/workarounds/inputs.scss'
import './../styles/workarounds/plaintext.scss'

const { string, bool, func } = PropTypes

export const defaultInputFluidProps = {
  fluid: true
}

export class VivaInput extends Component {
  componentDidMount () {
    if (this.props.mask) {
      __mask(this.props.mask)
    }
  }
  render () {
    const { placeholder, loading, disabled, fluid, error, value, name, mask } = this.props
    const props = { placeholder, loading, error, disabled, fluid }
    const errorStyle = {
      position: 'absolute',
      marginTop: '-27px',
      fontFamily: 'Roboto'
    }
    const inputStyle = {
      marginBottom: '30px'
    }
    const tealTextStyle = {
      position: 'absolute',
      marginTop: '-27px',
      fontFamily: 'Roboto'
    }
    const errorMessage = (
      <p style={errorStyle} className='error-message'>{this.props.messageText ? this.props.messageText : 'Введите корректные данные'}</p>
    )
    const additionalMessage = (
      <p style={tealTextStyle} className='additional-message'>{this.props.messageText ? this.props.messageText : 'Введите корректные данные'}</p>
    )

    return (
      <div>
        <Input style={inputStyle} className={mask} name={name} size='big' onInput={this.props.handleParentComponentStateChange} {...props} value={value} />
        {this.props.defaultMessage ? this.props.error ? errorMessage : additionalMessage : this.props.error && errorMessage}
      </div>
    )
  }
  static componentWillUnmount () {
    // document.querySelector(`div.${this.props.mask} input`).removeEventListener('input')
    // console.log(1234)
  }
}

VivaInput.propTypes = {
  placeholder: string,
  loading: bool,
  error: bool,
  disabled: bool,
  fluid: bool,
  value: string,
  handleParentComponentStateChange: func,
  name: string,
  mask: string,
  defaultMessage: bool,
  messageText: string
}
