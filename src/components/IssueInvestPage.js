import React, { Component } from 'react'
import { IssueInvestPageFormStepOne } from './IssueInvestPageFormStepOne'
import { IssueInvestPageFormStepTwo } from './IssueInvestPageFormStepTwo'
import { IssueInvestPageFormStepThree } from './IssueInvestPageFormStepThree'
import { VivaHeader } from './../ui-kit/VivaHeader'
import { VivaFooter } from './../ui-kit/VivaFooter'
import { VivaStepper } from './../ui-kit/VivaStepper'
import { __getToken,
  __createInvest,
  __createInvestCredit
  , __recoverPersonData,
  __getDictionaryList,
  __getCitiesListByName,
  __getCodesByValue,
  __submitCreditAndPerson,
  __getLoanCondition} from './../services/__api'
import { __dashToLodash, __camelToLodash, __dashLodashToCamel } from './../services/__filter'
import { __validateForm } from './../services/__validator'
import { __formatDictionaryToVivaDropdown, __formatCitiesToVivaAutocomplete, __formatCodesToVivaAutocomplete, __formatObjectPropsToCamel } from './../services/__data'
import { issuePageInvestModel, issuePageInvestCreditModel } from './../forms-models/issueInvestPage'
import moment from 'moment/src/moment'
import { appConfig } from './../configs/config'

export class IssueInvestPage extends Component {
  constructor () {
    super()
    this.state = {
      requestId: '',
      dictionary: {},
      citiesList: [],
      codesList: [],
      person: issuePageInvestModel,
      credit: issuePageInvestCreditModel,
      stepForm: 1,
      hasPhone: false,
      loanCondition: {},
      // replace with array
      registrationPrivateHome: false,
      factPrivateHome: false,
      isFactNotEqualReg: false,
      personCreated: false,
      creditCreated: false
    }

    this.handleIssueFormChange = this.handleIssueFormChange.bind(this)
    this.handleAsyncFormUpdate = this.handleAsyncFormUpdate.bind(this)
    this.handleSubmitPhone = this.handleSubmitPhone.bind(this)
    this.handleInvestChange = this.handleInvestChange.bind(this)
    this.handleInvestCreditChange = this.handleInvestCreditChange.bind(this)
    this.handleExtractPersonData = this.handleExtractPersonData.bind(this)
    this.handleExtractCreditData = this.handleExtractCreditData.bind(this)
    this.handleRegistrationPrivateHome = this.handleRegistrationPrivateHome.bind(this)
    this.handleFactPrivateHome = this.handleFactPrivateHome.bind(this)
    this.handleFactAddress = this.handleFactAddress.bind(this)
    this.handleApplyResponseValidations = this.handleApplyResponseValidations.bind(this)
    this.handleAutocompleteDrawCities = this.handleAutocompleteDrawCities.bind(this)
    this.handleAutocompleteDrawCodes = this.handleAutocompleteDrawCodes.bind(this)
    this.handleSubmitForm = this.handleSubmitForm.bind(this)
  }

  componentDidMount () {
    // add service parser!!!
    const hasToken = new RegExp(/token=/).test(document.cookie.toString())
    const token = document.cookie.replace(/token=/g, '')
    if (hasToken) {
      __recoverPersonData(token).then((res) => {
        // add checked flag(workaround!!!)
        this.handleExtractPersonData(res.data)
        this.handleExtractCreditData(res.data)
      })
    } else if (!hasToken) {
      __getToken().then((response) => {
        const issueToken = response.data.token
        // replace on service
        document.cookie = `token=${issueToken}; path=/; domain=${document.domain}; expires=Tue, 19 Jan 2038 03:14:07 GMT`
      })
    }
    // get dictionaryList
    __getDictionaryList().then((response) => {
      const list = __formatDictionaryToVivaDropdown(response.data['0'])
      this.setState({
        dictionary: list
      })
    })

    // get loan condition
    __getLoanCondition().then(response => {
      const loan = __formatObjectPropsToCamel(response.data['0'])
      this.setState({
        loanCondition: loan
      })
    })
  }

  handleAsyncFormUpdate (condition) {
    const issueToken = document.cookie.toString()
    // cancel previous asynchronous request
    clearTimeout(this.state.requestId)
    // add asynchronous request and save it number to state
    if (this.state.person.personalDataConsentInvest.value) {
      this.state.requestId = setTimeout(() => {
        if (condition === 'person') {
          __createInvest(this.state.person, issueToken).then((response) => {
            if (response.data.message.toUpperCase() === 'SUCCESS') {
              this.setState({
                personCreated: true
              })
            }
            this.handleApplyResponseValidations(response.data['validation_result'])
          })
        } else if (condition === 'credit') {
          __createInvestCredit(this.state.credit, issueToken).then((response) => {
            if (response.data.message.toUpperCase() === 'SUCCESS') {
              this.setState({
                creditCreated: true
              })
            }
            this.handleApplyResponseValidations(response.data['validation_result'])
          })
        }
      }, 3000)
    }
  }

  handleIssueFormChange (event, data) {
    // exception for fileloader - refactor!!!
    if (event.target.type === 'file') {
      const readURL = (file) => {
        const reader = new FileReader()
        reader.onload = (e) => {
          const key = __dashToLodash(event.target.name)
          const value = e.target.result.toString()
          const newState = {
            [key]: value
          }
          for (let prop in this.state.person) {
            if (__dashLodashToCamel(key) === prop) {
              this.handleInvestChange(newState)
            }
          }
          for (let prop in this.state.credit) {
            if (__dashLodashToCamel(key) === prop) {
              this.handleInvestCreditChange(newState)
            }
          }
        }
        reader.readAsDataURL(file)
      }
      event = {
        target: {
          name: event.target.name,
          value: event.target.files[0]
        }
      }
      readURL(event.target.value)
      //  deep refactor
    } else {
      const key = __dashToLodash(event.target.name || data.name)
      const value = event.target.value === 'checkbox' ? data : event.target.value || data.value
      const newState = {
        [key]: value
      }
      for (let prop in this.state.person) {
        if (__dashLodashToCamel(key) === prop) {
          this.handleInvestChange(newState)
        }
      }
      for (let prop in this.state.credit) {
        if (__dashLodashToCamel(key) === prop) {
          this.handleInvestCreditChange(newState)
        }
      }
    }
  }

  handleAutocompleteDrawCities (event) {
    const value = event.target.value
    if (value.length > 0) {
      __getCitiesListByName(event.target.value).then(response => {
        this.setState({
          citiesList: __formatCitiesToVivaAutocomplete(response.data)
        })
      })
    }
  }

  handleAutocompleteDrawCodes (event) {
    const value = event.target.value
    if (value.length > 0) {
      __getCodesByValue(event.target.value).then(response => {
        this.setState({
          codesList: __formatCodesToVivaAutocomplete(response.data)
        })
      })
    }
  }

  handleInvestChange (newPersonData) {
    const newState = Object.assign({}, this.state.person)
    for (let prop in this.state.person) {
      const underscoreProp = __camelToLodash(prop)
      if ((typeof newPersonData[underscoreProp] !== 'undefined') || newPersonData.hasOwnProperty(underscoreProp)) {
        newState[prop].value = newPersonData[underscoreProp]
        newState[prop].isValid = __validateForm(this.state.person[prop])
      }
    }
    this.setState({
      person: newState
    })
    this.handleAsyncFormUpdate('person')
  }

  handleInvestCreditChange (newCreditData) {
    const newState = Object.assign({}, this.state.credit)
    for (let prop in this.state.credit) {
      const underscoreProp = __camelToLodash(prop)
      if ((typeof newCreditData[underscoreProp] !== 'undefined') || newCreditData.hasOwnProperty(underscoreProp)) {
        newState['returnDate'].value =
          newCreditData['amount_days']
            ? moment().add(newCreditData['amount_days'], 'days').format('YYYY-MM-DD')
            : this.state.credit['returnDate'].value

        newState[prop].value = newCreditData[underscoreProp]
        newState[prop].isValid = __validateForm(this.state.credit[prop])
      }
    }
    this.setState({
      credit: newState
    })
    this.handleAsyncFormUpdate('credit')
  }

  handleSubmitForm () {
    // need for refactor
    const issueToken = document.cookie.toString().replace(/token=/g, '')
    __submitCreditAndPerson(issueToken).then(response => {
      if (response.data.message.toUpperCase() === 'SUCCESS') {
        localStorage.setItem('authToken', response.data['token'])
        document.location.href = `http://${appConfig.url}/#/personal`
      }
    })
  }

  handleExtractPersonData (newPersonData) {
    for (let prop in newPersonData) {
      const camelProp = __dashLodashToCamel(prop)
      const personProp = /:/g.test(camelProp) ? camelProp.replace(/:/g, '][').replace(/]/, '').concat(']') : camelProp
      if (this.state.person.hasOwnProperty(personProp)) {
        if (personProp === `address[${1 || 2}][compositeAddress][city]`) {
          this.setState({
            citiesList: [{
              key: newPersonData[prop],
              text: newPersonData[prop],
              value: newPersonData[prop]
            }]
          })
        } else if (personProp === `address[${1 || 2}][compositeAddress][zipCode]`) {
          this.setState({
            codesList: [{
              key: newPersonData[prop],
              text: newPersonData[prop],
              value: newPersonData[prop]
            }]
          })
        } else if (personProp === `birthday` || `identityDocument[expirationDate]`) {
          this.state.person[personProp] = {
            key: newPersonData[prop],
            text: newPersonData[prop],
            value: newPersonData[prop].split(/-/g).reverse().join('-')
          }
        }
        this.state.person[personProp].value = newPersonData[prop]
      }
    }
    this.setState({
      person: this.state.person
    })
  }

  handleExtractCreditData (newCreditData) {
    console.log('not working')
  }

  handleApplyResponseValidations (validations) {
    for (let prop in validations) {
      if (validations.hasOwnProperty(prop) && this.state.person.hasOwnProperty(__dashLodashToCamel(prop))) {
        const personProp = __dashLodashToCamel(prop)
        this.state.person[personProp].isValid = validations[prop]
      }
    }
    this.setState({
      person: this.state.person
    })
  }

  handleSubmitPhone () {
    this.setState({
      hasPhone: !this.state.hasPhone
    })
  }

  handleRegistrationPrivateHome () {
    this.setState({
      registrationPrivateHome: !this.state.registrationPrivateHome
    })
  }

  handleFactPrivateHome () {
    this.setState({
      factPrivateHome: !this.state.factPrivateHome
    })
  }

  handleFactAddress () {
    this.setState({
      isFactNotEqualReg: !this.state.isFactNotEqualReg
    })
  }

  render () {
    const form1 = {
      label: 'Шаг 1',
      content: <IssueInvestPageFormStepOne handleSubmitPhone={this.handleSubmitPhone}
        handleIssueFormChange={this.handleIssueFormChange}
        {...this.state}
      />
    }
    const form2 = {
      label: 'Шаг 2',
      content: <IssueInvestPageFormStepTwo
        {...this.state}
        handleRegistrationPrivateHome={this.handleRegistrationPrivateHome}
        handleFactPrivateHome={this.handleFactPrivateHome}
        handleFactAddress={this.handleFactAddress}
        handleIssueFormChange={this.handleIssueFormChange}
        handleAutocompleteDrawCities={this.handleAutocompleteDrawCities}
        handleAutocompleteDrawCodes={this.handleAutocompleteDrawCodes}
      />
    }
    const form3 = {
      label: 'Шаг 3',
      content: <IssueInvestPageFormStepThree handleIssueFormChange={this.handleIssueFormChange} {...this.state}
      />
    }
    const form = [ form1, form2, form3 ]

    return (
      <div>
        <header id='header' className='header-page'>
          <VivaHeader theme={'invertedBackgroundTheme'} />
        </header>
        <section id='content' className='mb-70px form-page'>
          <h1>Регистрация инвестора</h1>
          <div className='form-bottom-container'>
            <VivaStepper isFinished={this.state.personCreated && this.state.creditCreated} handleSubmitForm={this.handleSubmitForm} content={form} />
          </div>{ /* form-bottom-container */ }
        </section>{ /* content */ }
        <div className='container'>
          <VivaFooter />
        </div>
      </div>
    )
  }
}
