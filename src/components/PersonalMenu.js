import React, { Component } from 'react'
import Paper from 'material-ui/Paper'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme'

const style = {
  display: 'inline-block',
  margin: '16px 32px 16px 0',
  width: '100%'
}

export class PersonalMenu extends Component {
  render () {
    return (
      <MuiThemeProvider theme={getMuiTheme(darkBaseTheme)}>
        <Paper style={style}>
          <Menu>
            <MenuItem primaryText='Maps' />
            <MenuItem primaryText='Books' />
            <MenuItem primaryText='Flights' />
            <MenuItem primaryText='Apps' />
          </Menu>
        </Paper>
      </MuiThemeProvider>
    )
  }
}
