import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { VivaHeader } from './../ui-kit/VivaHeader'
import { VivaFooter } from './../ui-kit/VivaFooter'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import { VivaButton, tealButtonFluidProps } from './../ui-kit/VivaButton'
import { VivaPaperMessage, defaultSuccessProps } from './../ui-kit/VivaPaperMessage'
import { VivaIconMaterialWarning } from './../ui-kit/VivaIcons'

export class LogInPage extends Component {
  render () {

    const centerText = {
      textAlign: 'center',
      marginBottom: '6px',
      fontSize: '18px'
    }
    const theme = 'invertedBackgroundTheme'
    return (
      <div>
        <VivaHeader theme={theme} />
        <div className='container-fluid'>
          <div className=' col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 col-xs-12'>
            <div className='row mb-30px'>
              <div className='col-xs-12'>
                <div className='page-header'>
                  <h2>Вход в личный кабинет</h2>
                  <h3>
                    <small>Если вы клиент, введите логин. Если еще нет — телефон</small>
                  </h3>
                </div>
              </div>
              <div className='col-xs-12'>
                <VivaPaperMessage {...defaultSuccessProps} theme={theme} leftIcon={<VivaIconMaterialWarning theme={theme} constrainToInput />} >
                  Сообщение об успехе
                </VivaPaperMessage>
              </div>
            </div>
            <div className='col-sm-offset-3 col-sm-6 col-xs-12 '>
              <p style={centerText}>Логин/телефон</p>
              <VivaInput {...defaultInputFluidProps} placeholder={'Введите имя пользователя/телефон'} />
            </div>
            <div className='col-sm-offset-3 col-sm-6 col-xs-12 '>
              <p style={centerText}>Пароль</p>
              <VivaInput {...defaultInputFluidProps} placeholder={'Введите Пароль'} />
            </div>
            <div className='col-sm-offset-3 col-sm-6 col-xs-12 '>
              <VivaButton {...tealButtonFluidProps} value={<Link to='/personal'>ВОЙТИ</Link>} />
            </div>
            <VivaFooter />
          </div>
        </div>
      </div>
    )
  }
}
