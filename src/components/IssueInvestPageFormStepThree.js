import React, { Component, PropTypes } from 'react'
import { VivaCheckbox } from './../ui-kit/VivaCheckbox'
import { componentThemes } from './../ui-kit/colors/variables'

const { defaultTheme } = componentThemes
const { $primaryColor1 } = defaultTheme

export class IssueInvestPageFormStepThree extends Component {
  render () {
    const props = this.props
    const mainBlock = {
      border: `1px solid ${$primaryColor1}`,
      padding: '18px 25px'
    }
    const checkBoxMargin = {
      marginTop: '15px'
    }
    return (
      <section>
        <div className='container'>
          <div className='row'>
            <div style={mainBlock} className='col-sm-offset-2 col-sm-8'>
              <div style={checkBoxMargin} className='check-block'>
                <VivaCheckbox checked={props.person.personalAgreementConsentInvest} />
                <label style={{verticalAlign: 'super', paddingLeft: '0', width: 'calc(100% - 80px)', display: 'inline'}} className='check'>
                  Я хочу <a href='#'>инвестировать в вашу компанию</a>
                </label>
              </div>
            </div>{ /* col-sm-8 */ }
          </div>{ /* row */ }
        </div>{ /* container */ }
      </section>
    )
  }
}
