import React, { Component } from 'react'
import { VivaPaper } from './../ui-kit/VivaPaper'
import { VivaButtonGroup } from './../ui-kit/VivaButtonGroup'
import { VivaButton, extendedButtonFluidProps, defaultButtonFluidProps, tealButtonFluidProps } from './../ui-kit/VivaButton'
import { VivaCalendar } from './../ui-kit/VivaCalendar'
import { VivaDropdown, defaultAutocompleteFluidProps } from './../ui-kit/VivaDropdown'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'

export class UserFormData extends Component {
  render () {
    const mars = <span><i style={{fontSize: '18px', marginTop: '-20px'}} className='fa fa-mars' aria-hidden='true' />&nbsp;МУЖ</span>
    const venera = <span><i style={{fontSize: '18px', marginTop: '-20px'}} className='fa fa-venus' aria-hidden='true' />&nbsp;ЖЕН</span>
    const sexType = [{view: mars, model: '1'}, {view: venera, model: '2'}]
    return (
      <VivaPaper height='200px'>
        <div>
          <h3>Личные данные</h3>
          <div className='row'>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>фамилия</label>
                <VivaInput {...defaultInputFluidProps} name='second-name' placeholder={'Введите фамилию'} />
              </div>{ /* f-inp */ }
            </div>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>имя</label>
                <VivaInput {...defaultInputFluidProps} name='first-name' placeholder={'Введите имя'} />
              </div>{ /* f-inp */ }
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>отчество</label>
                <VivaInput {...defaultInputFluidProps} name='' {...defaultInputFluidProps} placeholder={'Введите отчество'} />
              </div>{ /* f-inp */ }
            </div>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>пол</label>
                { /** todo: удалить к ебеням иконки и вынести их в виде отдельных компонентов */ }
                <VivaButtonGroup name='sex-id' {...defaultButtonFluidProps} valueSet={sexType} />
              </div>{ /* f-inp */ }
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>Дата рождения</label>
                <VivaCalendar value='' name='birthday' {...defaultButtonFluidProps} valueSet='simpledate' />
                <div className='input-dop-info'>Возраст заёмщика должен быть от 18 до 75 полных лет</div>
              </div>{ /* f-inp */ }
            </div>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>Город</label>
                <VivaDropdown {...defaultAutocompleteFluidProps} placeholder={'Введите город'} name={`address[1][composite-address][city]`}
                />
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <div className='label-block clearfix'>
                  <label htmlFor className='pull-left'>Телефон</label>
                  <a style={{cursor: 'pointer'}} className='not pull-right'>Изменить</a>
                </div>
                <VivaInput mask='mask-phone' {...defaultInputFluidProps} name='contact[1][value]' placeholder={'Введите телефон'} />
              </div>{ /* f-inp */ }
            </div>{ /* col-sm-6 */ }
            <div className='col-sm-6'>
              <div className='f-inp' style={{marginTop: '26px'}}>
                <VivaButton {...extendedButtonFluidProps} value={'Подтвердить телефон'} />
              </div>
            </div>{ /* col-sm-6 */ }
          </div>
          <div className='row'>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <div className='label-block clearfix'>
                  <label htmlFor className='pull-left'>E-mail</label>
                  <a style={{cursor: 'pointer'}} className='not pull-right'>Изменить</a>
                </div>
                <VivaInput {...defaultInputFluidProps} name='contact[2][value]' placeholder={'Введите Email'} />
              </div>{ /* f-inp */ }
            </div>{ /* col-sm-6 */ }
            <div className='col-sm-6'>
              <div className='f-inp' style={{marginTop: '26px'}}>
                <VivaButton {...extendedButtonFluidProps} value={'Подтвердить E-mail'} />
              </div>
            </div>
          </div>
          <h3>Паспортные данные</h3>
          <div className='row'>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>Номер паспорта</label>
                <VivaInput {...defaultInputFluidProps} name='identity-document[number]' placeholder={'Введите данные'} />
              </div>{ /* f-inp */ }
            </div>{ /* col-sm-6 */ }
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>Место рождения</label>
                <VivaInput {...defaultInputFluidProps} name='birth-place' placeholder={'Введите данные'} />
                <div className='input-dop-info'>Введите современное название населенного пункта</div>
              </div>{ /* f-inp */ }
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>Код подразделения</label>
                <VivaInput {...defaultInputFluidProps} name='' placeholder={'Введите данные'} />
              </div>{ /* f-inp */ }
            </div>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>Дата окончания</label>
                <VivaCalendar value='' name='identity-document[expiration-date]' {...defaultButtonFluidProps} valueSet='simpledate' />
              </div>{ /* f-inp */ }
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-12'>
              <div className='f-inp'>
                <label htmlFor>Кем выдан</label>
                <div className='well'>ОТДЕЛЕНИЕМ УФМС РОССИИ ПО ГОР. МОСКВЕ ПО РАЙОНУ КОПТЕВО</div>
              </div>
            </div>
          </div>
          <h3>Адрес регистрации</h3>
          <div className='row'>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>Город</label>
                <VivaDropdown {...defaultAutocompleteFluidProps} placeholder={'Введите город'} name={`address[1][composite-address][city]`}
                />
              </div>
            </div>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <label htmlFor>индекс</label>
                <VivaDropdown {...defaultAutocompleteFluidProps} placeholder={'Введите индекс'} name={`address[1][composite-address][zip-code]`}
                />
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-12'>
              <div className='f-inp'>
                <label htmlFor>Улица</label>
                <VivaInput {...defaultInputFluidProps} placeholder={'Введите город'} name={`address[1][composite-address][street]`}
                />
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-3'>
              <div className='f-inp'>
                <label htmlFor>№ Дома</label>
                <VivaInput {...defaultInputFluidProps} name='address[composite-address][house-number]' placeholder={''} />
              </div>
            </div>
            <div className='col-sm-3'>
              <div className='f-inp'>
                <label htmlFor>корпус</label>
                <VivaInput {...defaultInputFluidProps} name='' placeholder={''} />
              </div>
            </div>
            <div className='col-sm-3'>
              <div className='f-inp'>
                <label htmlFor>строение</label>
                <VivaInput {...defaultInputFluidProps} name='' placeholder={''} />
              </div>
            </div>
            <div className='col-sm-3'>
              <div className='f-inp'>
                <label htmlFor>квартира</label>
                <VivaInput {...defaultInputFluidProps} name={`address[1][composite-address][flat]`} placeholder={''} />
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <VivaButton {...tealButtonFluidProps} value='Принять' />
              </div>
            </div>
            <div className='col-sm-6'>
              <div className='f-inp'>
                <VivaButton {...defaultButtonFluidProps} value='Отказаться' />
              </div>
            </div>
          </div>
        </div>
      </VivaPaper>
    )
  }
}
