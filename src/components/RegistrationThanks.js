import React, { Component } from 'react'
import { VivaPaper } from '../ui-kit/VivaPaper'
import { VivaButton, extendedButtonFluidProps } from './../ui-kit/VivaButton'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import {VivaCheckbox} from '../ui-kit/VivaCheckbox'
import URLSearchParams from 'url-search-params'
import { appConfig } from './../configs/config'
import axios from 'axios'

export class RegistrationThanks extends Component {
  constructor () {
    super()
    this.state = {
      code: ''
    }
  }

  render () {
    const checkboxLabelStyle = {verticalAlign: 'super', paddingLeft: '0', width: 'calc(100% - 80px)', display: 'inline'}
    const pointerStyle = {
      cursor: 'pointer'
    }
    return (
      <VivaPaper>
        <div className='row'>
          <div className='col-md-12'>
            <h3>Подписание договора</h3>
            <p>Пожалуйста, подпишите договор</p>
            <div className='row mb-10px'>
              <div className='col-sm-6'>
                <div className='f-inp'>
                  <label htmlFor>ТЕЛЕФОН</label>
                  <a style={pointerStyle} className='not pull-right'>Изменить</a>
                  <div className='well well-sm'>+7 (999) 123-45-67</div>
                </div>
              </div>
              <div className='col-sm-6'>
                <div className='f-inp'>
                  <label htmlFor>КОД ИЗ СМС <a href='#'>?</a></label>
                  <a style={pointerStyle} className='not pull-right'>Изменить</a>
                  <VivaInput {...defaultInputFluidProps} value={this.state.code} handleParentComponentStateChange={(event) => {
                    console.log(event.target.value)
                    this.setState({
                      code: event.target.value
                    })
                  }} placeholder='СМС Код' />
                </div>
              </div>
            </div>
            <div className='check-block mb-10px'>
              <VivaCheckbox />
              <label style={checkboxLabelStyle} className='check'>
                Я даю согласие на обработку моих персональных данных, а также даю согласие на запрос данных из <a href='#'>БКИ</a>
              </label>
            </div>
            <div className='check-block mb-10px'>
              <VivaCheckbox />
              <label style={checkboxLabelStyle} className='check'>
                Я разрешаю присылать мне специальные предложения и новости по электронной почте или СМС
              </label>
            </div>
            <div className='col-sm-12'>
              <div className='f-inp' style={{marginTop: '26px'}}>
                <VivaButton {...extendedButtonFluidProps} value={'Подписать'} handleParentComponentStateChange={() => {
                  let params = new URLSearchParams()
                  params.append('person_token', document.cookie.replace(/token=/g, ''))
                  params.append('code', this.state.code)
                    // TODO Set to global state in init step
                  axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('authToken')}`

                  return axios.post(`http://${appConfig.apiUrl}/api/sign/confirmCredit`, params, {withCredentials: true}).then()
                }} />
              </div>
            </div>
          </div>
        </div>
      </VivaPaper>
    )
  }
}
