import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { VivaPaper } from './../ui-kit/VivaPaper'
import { VivaButton, tealButtonFluidProps } from './../ui-kit/VivaButton'
import { VivaDropdown, defaultDropdownFluidProps } from './../ui-kit/VivaDropdown'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import { VivaTextArea, defaultTextAreaFluidProps } from './../ui-kit/VivaTextArea'

export class HotLine extends Component {
  render () {
    return (
      <VivaPaper height='200px'>
        <div>
          <h3>Горячая линия</h3>
          <div className='row'>
            <div className='col-sm-12'>
              <div className='f-inp'>
                <label htmlFor>тема</label>
                <VivaDropdown {...defaultDropdownFluidProps} placeholder={'Выбрать категорию'} name='type' />
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-12'>
              <div className='f-inp'>
                <label htmlFor>заголовок</label>
                <VivaInput {...defaultInputFluidProps} name='first-name' placeholder={'Введите данные'} />
              </div>{ /* f-inp */ }
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-12'>
              <div className='f-inp'>
                <label htmlFor>обращение</label>
                <VivaTextArea {...defaultTextAreaFluidProps} rows='3' placeholder='Текст обращения' />
              </div>
            </div>
          </div>
          <div className='row mb-10px'>
            <div className='col-sm-12'>
              <div className='row mb-10px'>
                <div className='col-sm-9'>
                  График платежей <span className='label label-default'>PDF</span>
                </div>
                <div className='col-sm-1'>
                  <Link to='#'>УДАЛИТЬ</Link>
                </div>
              </div>
            </div>
          </div>
          <div className='row mb-10px'>
            <div className='col-sm-6'>
              <Link to='#'>ПРИКРЕПИТЬ ФАЙЛ</Link>
            </div>
          </div>
          <div className='row'>
            <div className='col-sm-6'>
              <VivaButton {...tealButtonFluidProps} value='Отправить' />
            </div>
          </div>
        </div>
      </VivaPaper>
    )
  }
}
