import React  from 'react'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import { VivaButton, extendedButtonFluidProps } from './../ui-kit/VivaButton'

export const IssuePageConfirmPhone = props => (
  <div>
    {props.hasPhone && (
      <div className='f-inp'>
        <div className='label-block clearfix'>
          <label htmlFor className='pull-left'>Введите код из СМС:</label>
          <a className='not pull-right'>Отправить повторно</a>
        </div>
        <div className='row'>
          <div className='col-sm-4'>
            <VivaInput {...defaultInputFluidProps} handleParentComponentStateChange={props.handleIssueFormChange} />
          </div>
          <div className='col-sm-8'>
            <VivaButton {...extendedButtonFluidProps} handleParentComponentStateChange={props.handleSubmitPhone} value={'Подтвердить код'} />
          </div>
        </div>
      </div>
    )}
    {!props.hasPhone && (
      <div className='f-inp' style={{marginTop: '26px'}}>
        <VivaButton {...extendedButtonFluidProps} handleParentComponentStateChange={props.handleSubmitPhone} value={'Подтвердить телефон'} />
      </div>
    )}
  </div>
)
