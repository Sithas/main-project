import React, { Component, PropTypes } from 'react'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import { VivaCalendar, defaultButtonFluidProps } from './../ui-kit/VivaCalendar'

export class IssuePageBankCardData extends Component {
  render () {
    const props = this.props
    const credit = this.props.credit
    return (
      <div className='tab-pane active' id='home'>
        <div className='container'>
          <div className='row mb-40px'>
            <div className='col-sm-offset-2 col-sm-8'>
              <div className='row'>
                <div className='col-sm-8'>
                  <div className='f-inp'>
                    <label htmlFor>Номер карты</label>
                    <div className='cart-input'>
                      <VivaInput
                        mask='mask-card'
                        {...defaultInputFluidProps} name='requisite[card-number]'
                        error={!credit['requisite[cardNumber]'].isValid} value={credit['requisite[cardNumber]'].value}
                        handleParentComponentStateChange={props.handleIssueFormChange} placeholder={'XXXX XXXX XXXX XXXX'} />
                    </div>
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
                <div className='col-sm-4'>
                  <div className='f-inp'>
                    <label htmlFor>Действителен до</label>
                    <VivaCalendar name='requisite[expiration-date]' value={credit['requisite[expirationDate]'].value} {...defaultButtonFluidProps} handleParentComponentStateChange={props.handleIssueFormChange} valueSet='bankcard' />
                    <div className='input-dop-info'>Возраст заёмщика должен быть от 18 до 75 полных лет</div>
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
                <div className='col-sm-8'>
                  <div className='f-inp'>
                    <label htmlFor>Держатель карты</label>
                    <div className='cart-input'>
                      <VivaInput {...defaultInputFluidProps}name='requisite[holder-name-surname]'
                        error={!credit['requisite[holderNameSurname]'].isValid} value={credit['requisite[holderNameSurname]'].value.length > 0 ? credit['requisite[holderNameSurname]'].value.toUpperCase() : ''}
                        handleParentComponentStateChange={props.handleIssueFormChange}
                        placeholder={'MR CARDHOLDER'} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
