import React, { Component, PropTypes } from 'react'
import { IssuePageConfirmPhone } from './IssuePageConfirmPhone'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import { defaultButtonFluidProps } from './../ui-kit/VivaButton'
import { VivaButtonGroup } from './../ui-kit/VivaButtonGroup'
import { VivaCalendar } from './../ui-kit/VivaCalendar'
import { VivaCheckbox } from './../ui-kit/VivaCheckbox'

const { any } = PropTypes

export class IssuePageFormStepOne extends Component {
  render () {
    const props = this.props
    const person = this.props.person
    const mars = <span><i style={{fontSize: '18px', marginTop: '-20px'}} className='fa fa-mars' aria-hidden='true' />&nbsp;МУЖ</span>
    const venera = <span><i style={{fontSize: '18px', marginTop: '-20px'}} className='fa fa-venus' aria-hidden='true' />&nbsp;ЖЕН</span>
    const sexType = [{view: mars, model: '1'}, {view: venera, model: '2'}]
    return (
      <section>
        <div className='container'>
          <div className='row'>
            <div className='col-sm-offset-2 col-sm-8'>
              <div className='row'>
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <label htmlFor>фамилия</label>
                    <VivaInput {...defaultInputFluidProps} name='second-name' error={!person.secondName.isValid} value={person.secondName.value} handleParentComponentStateChange={props.handleIssueFormChange} placeholder={'Введите фамилию'} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <label htmlFor>имя</label>
                    <VivaInput {...defaultInputFluidProps} name='first-name' error={!person.firstName.isValid} value={person.firstName.value} handleParentComponentStateChange={props.handleIssueFormChange} placeholder={'Введите имя'} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
              </div>{ /* row */ }
              <div className='row mb-40px'>
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <label htmlFor>Персональный номер</label>
                    <VivaInput {...defaultInputFluidProps} name='identification-number' error={!person.identificationNumber.isValid} value={person.identificationNumber.value} handleParentComponentStateChange={props.handleIssueFormChange} {...defaultInputFluidProps} placeholder={'Введите номер'} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <label htmlFor>пол</label>
                    { /** todo: удалить к ебеням иконки и вынести их в виде отдельных компонентов */ }
                    <VivaButtonGroup name='sex-id' value={person.sexId.value}
                      handleParentComponentStateChange={props.handleIssueFormChange
                      } activeCondition={person.sexId.value} {...defaultButtonFluidProps} valueSet={sexType} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
              </div>{ /* row */ }
              <div className='row mb-40px'>
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <label htmlFor>Дата рождения</label>
                    <VivaCalendar name='birthday' value={person.birthday.value} {...defaultButtonFluidProps} handleParentComponentStateChange={props.handleIssueFormChange} valueSet='simpledate' />
                    <div className='input-dop-info'>Возраст заёмщика должен быть от 18 до 75 полных лет</div>
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
              </div>{ /* row */ }
              <div className='row mb-40px'>
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <div className='label-block clearfix'>
                      <label htmlFor className='pull-left'>Телефон</label>
                    </div>
                    <VivaInput mask='mask-phone' {...defaultInputFluidProps} name='contact[1][value]' error={!person['contact[1][value]'].isValid} value={person['contact[1][value]'].value} handleParentComponentStateChange={props.handleIssueFormChange} placeholder={'Введите телефон'} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
                <div className='col-sm-6'>
                  <IssuePageConfirmPhone {...props} />
                </div>{ /* col-sm-6 */ }
              </div>{ /* row */ }
              <div className='row mb-40px'>
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <div className='label-block clearfix'>
                      <label htmlFor className='pull-left'>E-mail</label>
                      <a style={{cursor: 'pointer'}} className='not pull-right'>У меня нет E-mail</a>
                    </div>
                    <VivaInput {...defaultInputFluidProps} name='contact[2][value]' error={!person['contact[2][value]'].isValid} messageText='Введите корректный E-mail' value={person['contact[2][value]'].value} handleParentComponentStateChange={props.handleIssueFormChange} placeholder={'Введите Email'} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
              </div>{ /* row */ }
              <div className='check-block mb-20px'>
                <VivaCheckbox checked={props.person.personalDataConsent.value} name='personal-data-consent' handleParentComponentStateChange={props.handleIssueFormChange} />
                <label style={{verticalAlign: 'super', paddingLeft: '0', width: 'calc(100% - 80px)', display: 'inline'}} className='check'>
                  Я даю согласие на обработку моих персональных данных
                </label>
              </div>
              <div className='check-block mb-20px'>
                <VivaCheckbox checked={props.person.creditHistoryRequestConsent.value} name='credit-history-request-consent' handleParentComponentStateChange={props.handleIssueFormChange} />
                <label style={{verticalAlign: 'super', paddingLeft: '0', width: 'calc(100% - 80px)', display: 'inline'}} className='check'>
                  Я даю согласие на обработку моих персональных данных,
                  а также даю согласие на запрос данных из <a href='#'>БКИ</a>
                </label>
              </div>
              <div className='check-block mb-40px'>
                <VivaCheckbox checked={props.person.receivingAdConsent.value} name='receiving-ad-consent' handleParentComponentStateChange={props.handleIssueFormChange} />
                <label style={{verticalAlign: 'super', paddingLeft: '0', width: 'calc(100% - 80px)', display: 'inline'}} className='check'>
                  Я разрешаю присылать мне специальные предложения
                  и новости по электронной почте или СМС
                </label>
              </div>
            </div>{ /* col-sm-8 */ }
          </div>{ /* row */ }
        </div>{ /* container */ }
      </section>
    )
  }
}

IssuePageFormStepOne.propTypes = {
  handleSubmitPhone: any,
  person: any,
  hasPhone: any,
  handleIssueFormChange: any
}
