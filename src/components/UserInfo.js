import React, { Component } from 'react'
import { VivaPaper } from './../ui-kit/VivaPaper'

export class UserInfo extends Component {
  render () {
    return (
      <VivaPaper>
        <div>
          <h3>Алексей Вагнер</h3>
          <h4>
            <small>Vagner@gmail.com</small>
          </h4>
          <h4>
            <small>+420(485) 123-456-34</small>
          </h4>
          <h4>
            <small>Еще немного информации</small>
          </h4>
          <h4>
            <small>Еще немного информации</small>
          </h4>
          <h4>
            <small>Еще немного информации</small>
          </h4>
        </div>
      </VivaPaper>
    )
  }
}
