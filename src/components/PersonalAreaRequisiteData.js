import React, { Component } from 'react'
import { VivaPaper } from '../ui-kit/VivaPaper'
import { VivaButtonGroup } from '../ui-kit/VivaButtonGroup'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import { VivaCalendar, defaultButtonFluidProps } from './../ui-kit/VivaCalendar'
import { VivaFileUploader } from '../ui-kit/VivaFileUploader'
import { VivaButton, tealButtonFluidProps } from '../ui-kit/VivaButton'
import { VivaSliderInput } from './../ui-kit/VivaSliderInput'
import { VivaPaperMessage, defaultSuccessProps } from './../ui-kit/VivaPaperMessage'
import { VivaIconMaterialWarning } from './../ui-kit/VivaIcons'

export class PersonalAreaRequisiteDataBank extends Component {
  render () {
    const card = <span style={{overflow: 'hidden', textOverflow: 'ellipsis'}}><i style={{fontSize: '18px', marginTop: '-20px'}} className='fa fa-credit-card' aria-hidden='true' />&nbsp;БАНКОВСКАЯ КАРТА</span>
    const bank = <span style={{overflow: 'hidden', textOverflow: 'ellipsis'}}><i style={{fontSize: '18px', marginTop: '-20px'}} className='fa fa-university' aria-hidden='true' />&nbsp;БАНКОВСКИЙ СЧЕТ</span>
    const amountType = [{view: card, model: '1'}, {view: bank, model: '2'}]
    return (
      <VivaPaper>
        <div className='row'>
          <div className='col-md-10 col-md-offset-1'>
            <div className='col-md-1 col-md-offset-12'>
              <button type='button' className='close' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='row'>
              <div className='col-xs-12'>
                <div className='f-inp'>
                  <h3>Добавить реквизиты</h3>
                  <VivaButtonGroup valueSet={amountType} {...defaultButtonFluidProps} name='requisite[type]' />
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-xs-12'>
                <div className='row'>
                  <div className='col-sm-12'>
                    <div className='f-inp'>
                      <label htmlFor>БИК банка <a href='#'>?</a></label>
                      <VivaInput {...defaultInputFluidProps} name='requisite[card-number]' />
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-sm-12'>
                    <div className='f-inp'>
                      <label htmlFor>Счет банка <a href='#'>?</a></label>
                      <VivaInput {...defaultInputFluidProps} />
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-sm-12'>
                    <div className='f-inp'>
                      <VivaButton {...tealButtonFluidProps} value='Добавить' />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </VivaPaper>
    )
  }
}

export class PersonalAreaRequisiteDataCard extends Component {
  render () {
    const card = <span style={{overflow: 'hidden', textOverflow: 'ellipsis'}}><i style={{fontSize: '18px', marginTop: '-20px'}} className='fa fa-credit-card' aria-hidden='true' />&nbsp;БАНКОВСКАЯ КАРТА</span>
    const bank = <span style={{overflow: 'hidden', textOverflow: 'ellipsis'}}><i style={{fontSize: '18px', marginTop: '-20px'}} className='fa fa-university' aria-hidden='true' />&nbsp;БАНКОВСКИЙ СЧЕТ</span>
    const amountType = [{view: card, model: '1'}, {view: bank, model: '2'}]
    return (
      <VivaPaper>
        <div className='row'>
          <div className='col-md-10 col-md-offset-1'>
            <div className='col-md-1 col-md-offset-12'>
              <button type='button' className='close' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='row'>
              <div className='col-xs-12'>
                <div className='f-inp'>
                  <h3>Добавить реквизиты</h3>
                  <VivaButtonGroup valueSet={amountType} {...defaultButtonFluidProps} name='requisite[type]' />
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-xs-12'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <div className='f-inp'>
                      <label htmlFor>номер карты</label>
                      <VivaInput {...defaultInputFluidProps} name='requisite[card-number]' />
                    </div>
                  </div>
                  <div className='col-sm-4'>
                    <div className='f-inp'>
                      <label htmlFor>Действителен до</label>
                      <VivaCalendar name='requisite[expiration-date]' value='0' {...defaultButtonFluidProps} valueSet='bankcard' />
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-sm-8'>
                    <div className='f-inp'>
                      <label htmlFor>держатель карты</label>
                      <VivaInput {...defaultInputFluidProps} defaultMessage messageText='Карта должна быть именной и принадлежать заявителю' name='requisite[card-holder]' />
                    </div>
                  </div>
                  <div className='col-sm-4'>
                    <div className='f-inp'>
                      <label htmlFor>фото карты <a href='#'>?</a></label>
                      <VivaFileUploader name='identity-document-image-front' />
                    </div>
                  </div>
                </div>
                <div className='row mb-15px'>
                  <div className='col-sm-12'>
                    <p>Фотография лицевой стороны вашей карты нам необходима для того чтобы удостовериться в том что данные введеные выше соответсвуют действительности и имя заявителя и деражателя карты совпадают.<br />Мы также хотим обратить ваше внимание, что просим приложить фото только лицевой стороны карты и не просим вас указать код безопасности, который находиться на обратной стороне карты. Так что вы можете не беспокоиться, вашими платежными реквизитами никто не сможет воспользоваться.</p>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-sm-12'>
                    <div className='f-inp'>
                      <VivaButton {...tealButtonFluidProps} value='Добавить' />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </VivaPaper>
    )
  }
}

export class PersonalAreaRequisiteDataCredit extends Component {
  render () {
    return (
      <VivaPaper>
        <div className='row'>
          <div className='col-md-10 col-md-offset-1'>

          <div className='col-md-1 col-md-offset-12'>
            <button type='button' className='close' aria-label='Close'>
              <span aria-hidden='true'>&times;</span>
            </button>
          </div>

                 <div className='row'>
                <h3>Текущий заём</h3>
                <div className='col-sm-12'>
                  <div className='label-polz clearfix mb-10px'>
                    <div className='label-zaym'>Сумма платежа</div>
                  </div>{ /* label-polz */ }
                  <VivaSliderInput
                    name={'amount'}
                    value={5}
                    color={'#000000'}
                    min={1}
                    max={23}
                    step={1} />
                </div>
                <h3>Оплатить с помощью</h3>

            <div className='col-sm-12'>
              <div className='f-inp'>
                <VivaButton {...tealButtonFluidProps} value='Оплатить' />
              </div>
            </div>
          </div>
      

          </div>
        </div>
      </VivaPaper>
    )
  }
}
