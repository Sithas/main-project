import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { VivaPaper } from './../ui-kit/VivaPaper'

export class MessageArchive extends Component {
  render () {
    return (
      <VivaPaper>
        <div>
          <h3>Архив сообщений</h3>
          <div className='row'>
            <div className='col-md-4'>
              Обращение 112200
            </div>
            <div className='col-md-3'>
              <p>1 января 2017</p>
            </div>
            <div className='col-md-2'>
              <span className='type-request-close'>Закрыто</span>
            </div>
            <div className='col-md-3'>
              <Link to='see' htmlFor>{'смотреть'.toUpperCase()}</Link>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-4'>
              Обращение 112200
            </div>
            <div className='col-md-3'>
              <p>1 января 2017</p>
            </div>
            <div className='col-md-2'>
              Открыто
            </div>
            <div className='col-md-3'>
              <Link to='see' htmlFor>{'смотреть'.toUpperCase()}</Link>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-4'>
              Обращение 112200
            </div>
            <div className='col-md-3'>
              <p>1 января 2017</p>
            </div>
            <div className='col-md-2'>
              Открыто
            </div>
            <div className='col-md-3'>
              <Link to='see' htmlFor>{'смотреть'.toUpperCase()}</Link>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-4'>
              Обращение 112200
            </div>
            <div className='col-md-3'>
              <p>1 января 2017</p>
            </div>
            <div className='col-md-2'>
              Открыто
            </div>
            <div className='col-md-3'>
              <Link to='see' htmlFor>{'смотреть'.toUpperCase()}</Link>
            </div>
          </div>
        </div>
      </VivaPaper>
    )
  }
}
