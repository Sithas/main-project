import React, { Component } from 'react'
import { VivaSliderInput } from './../ui-kit/VivaSliderInput'
import moment from 'moment/src/moment'
import { __calculatePercentByDays } from './../services/__data'
import { __formatMoney } from './../services/__filter'

export class IssuePageCreditProperties extends Component {
  render () {
    const props = this.props
    const credit = this.props.credit
    const returnAmount = __calculatePercentByDays(Number(credit['amount'].value), credit['amountDays'].value, props.loanCondition.percent)
    return (
      <div className='form-page-container'>
        <div className='container'>
          <div className='row'>
            <div className='col-sm-8 col-sm-offset-2'>
              <div className='text-center'>
                <h1>Оформить займ</h1>
              </div>{ /* text-center */ }
              <div className='zaym-container'>
                <div className='row'>
                  <div className='col-sm-6'>
                    <div className='label-polz clearfix mb-10px'>
                      <div className='label-zaym'>Сумма займа</div>
                    </div>{ /* label-polz */ }
                    <VivaSliderInput
                      handleParentComponentStateChange={props.handleIssueFormChange}
                      name={'amount'} value={credit['amount'].value}
                      color={'#000000'}
                      min={props.loanCondition.amountMin / 100}
                      max={props.loanCondition.amountMax / 100} step={props.loanCondition.stepAmount / 100} />
                  </div>
                  <div className='col-sm-6'>
                    <div className='label-polz clearfix mb-10px'>
                      <div className='label-zaym'>Срок займа</div>
                    </div>{ /* label-polz */ }
                    <VivaSliderInput
                      handleParentComponentStateChange={props.handleIssueFormChange}
                      name={'amount-days'}
                      value={credit['amountDays'].value}
                      color={'#000000'} min={props.loanCondition.termMin} max={props.loanCondition.termMax} step={props.loanCondition.stepTerm} />
                  </div>
                </div>{ /* row */ }
                <div className='polz-result mb-30px'>
                  <div className='pz-title'>Возвращаете</div>
                  <div className='pz-price'>{
                    __formatMoney(returnAmount)
                  }</div>
                  <div className='pz-price'><h5>{moment().add(Number(credit['amountDays'].value), 'days').format('D MMMM YYYY')}</h5></div>
                </div>{ /* <div className='text-center'> <a href='#'>У меня есть промокод</a> </div> */ }
              </div>{ /* zaym-continer */ }
            </div>{ /* col-sm-8 */ }
          </div>{ /* row */ }
        </div>{ /* container */ }
      </div>
    )
  }
}
