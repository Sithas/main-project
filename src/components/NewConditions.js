import React, { Component } from 'react'
import { VivaPaper } from '../ui-kit/VivaPaper'
import { VivaButton, defaultButtonFluidProps, tealButtonFluidProps } from './../ui-kit/VivaButton'

export class NewConditions extends Component {
  render () {
    const greenText = {color: '#4ecbc4', marginBottom: '5px'}
    return (
      <VivaPaper>
        <div className='row'>
          <div className='col-md-12'>
            <h3>Новые условия</h3>
            <p>Мы готовы одобрить заём на меньшую сумму. Вам требуется нажать кнопку согласия или отказа с новыми условиями</p>
            <div className='row mb-20px'>
              <div className='col-sm-6 mb-5px'>
                <div className='row'>
                  <div className='col-sm-12 col-xs-6'>
                    <h4 className='text-left' style={greenText}>СУММА ЗАЙМА:</h4>
                  </div>
                  <div className='col-sm-12 col-xs-6'>
                    <h4 className='text-left'>1488 ₽</h4>
                  </div>
                </div>
              </div>
              <div className='col-sm-6 mb-5px'>
                <div className='row'>
                  <div className='col-sm-12 col-xs-6'>
                    <h4 style={greenText}>СРОК ПЛАТЕЖА</h4>
                  </div>
                  <div className='col-sm-12 col-xs-6'>
                    <h4 className='text-left'>11 СЕН 2001</h4>
                  </div>
                </div>
              </div>
              <div className='col-sm-6 mb-5px'>
                <VivaButton {...tealButtonFluidProps} value='Принять' />
              </div>
              <div className='col-sm-6 mb-5px'>
                <VivaButton {...defaultButtonFluidProps} value='Отказаться' />
              </div>
            </div>
            <div className='row mb-20px'>
              <div className='col-sm-6'>
                <p>Сумма займа:</p>
              </div>
              <div className='col-sm-6'>
                <h4>1488 ₽</h4>
              </div>
            </div>
            <div className='row mb-20px'>
              <div className='col-sm-6'>
                <p>Дата погашения:</p>
              </div>
              <div className='col-sm-6'>
                <h4>18 марта 2018</h4>
              </div>
            </div>
            <div className='row mb-20px'>
              <div className='col-sm-6'>
                <p>Процент:</p>
              </div>
              <div className='col-sm-6'>
                <h4>22,8%</h4>
              </div>
            </div>
          </div>
        </div>
      </VivaPaper>
    )
  }
}
