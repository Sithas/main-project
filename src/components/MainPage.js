import React, { Component } from 'react'
import { VivaHeader } from './../ui-kit/VivaHeader'
import { VivaButton, defaultButtonProps, defaultButtonFluidProps } from './../ui-kit/VivaButton'
import { VivaSliderInput } from './../ui-kit/VivaSliderInput'
import { VivaFooter } from './../ui-kit/VivaFooter'
import { Link } from 'react-router-dom'

export class MainPage extends Component {
  render () {
    return (
      <div>
        <header id='header' className='header-main'>
          <a href='#content' className='mouse hidden-xs hidden-sm'><img src='images/mouse.png' alt /></a>
          <VivaHeader theme={'invertedTheme'} />
          <div className='top-main-abs'>
            <div className='container'>
              <div className='col-sm-6'>
                <div className='main-title mb-40px'>Срочные займы<br /></div>
                <div className='main-dop-info'>
                  <p>Деньги срочно на карту или счет!</p>
                  <p>По паспорту, без залога и поручителей!</p>
                  <a href='#'>подробнее</a>
                </div>{/* main-fop-info */}
                <a href='#content' className='down visible-xs'><i className='fa fa-chevron-down' aria-hidden='true' /></a>
              </div>{/* col-sm-6 */}
              <div className='col-sm-offset-1 col-sm-4'>
                <div className='zaym-container'>
                  <div className='polz-block mb-30px'>
                    <div className='label-polz clearfix mb-10px'>
                      <div className='label-zaym'>Сумма займа</div>
                      <div className='label-price'>2000 - 50 000 <i className='fa fa-rub' aria-hidden='true' />
                      </div>
                    </div>{/* label-polz */}
                    <VivaSliderInput color={'#eee'} min={0} max={100000} step={1000} />
                  </div>{/* polz-block */}
                  <div className='polz-block mb-30px'>
                    <div className='label-polz clearfix mb-10px'>
                      <div className='label-zaym'>Срок займа</div>
                      <div className='label-price'>61 - 216 дней</div>
                    </div>{/* label-polz */}
                    <VivaSliderInput color={'#eee'} min={0} max={30} step={1} />
                  </div>
                  <div className='polz-result text-center mb-30px'>
                    <div className='pz-title'>Возвращаете</div>
                    <div className='pz-price'>34 000 <i className='fa fa-rub' aria-hidden='true' /></div>
                    <div className='pz-date'>12 Февраля 2017</div>
                  </div>{/* polz-result */}
                  <VivaButton {...defaultButtonFluidProps} value={<Link to='/issue'>ПОЛУЧИТЬ ДЕНЬГИ</Link>} />
                </div>{/* zaym-continer */}
              </div>{/* col-sm-6 */}
            </div>{/* container */}
          </div>{/* top-main-abs */}
          <a href='#content' className='down visible-sm'><i className='fa fa-chevron-down' aria-hidden='true' /></a>
        </header>{/* header */}
        <section id='content' className='mb-70px'>
          <div className='white-container scroll-cont'>
            <div className='container'>
              <div className='row hidden-xs'>
                <div className='col-sm-3'>
                  <div className='polz-block'>
                    <div className='label-polz clearfix mb-10px'>
                      <div className='label-zaym'>Сумма займа</div>
                    </div>{/* label-polz */}
                    <div className='calc-results-block'>
                      <div className='calc-minus hidden-sm' id='m3'>-</div>
                      <div className='calc-result' id='c3'>24 000 <i className='fa fa-rub' aria-hidden='true' /></div>
                      <div className='calc-plus hidden-sm' id='p3'>+</div>
                    </div>{/* calc-result-block */}
                    <div id='polzunok3' className='polzunok hidden-sm' />
                  </div>{/* polz-block */}
                </div>{/* col-sm-3 */}
                <div className='col-sm-3'>
                  <div className='polz-block'>
                    <div className='label-polz clearfix mb-10px'>
                      <div className='label-zaym'>Срок займа</div>
                    </div>{/* label-polz */}
                    <div className='calc-results-block'>
                      <div className='calc-minus hidden-sm' id='m4'>-</div>
                      <div className='calc-result' id='c4'>61 дней</div>
                      <div className='calc-plus hidden-sm' id='p4'>+</div>
                    </div>{/* calc-result-block */}
                    <div id='polzunok4' className='polzunok hidden-sm' />
                  </div>{/* polz-block */}
                </div>{/* col-sm-3 */}
                <div className='col-sm-3'>
                  <div className='polz-result text-center'>
                    <div className='pz-title'>Возвращаете</div>
                    <div className='pz-price'>34 000 <i className='fa fa-rub' aria-hidden='true' /></div>
                    <div className='pz-date'>12 Февраля 2017</div>
                  </div>{/* polz-result */}
                </div>{/* col-sm-3 */}
                <div className='col-sm-3'>
                  <a href='#' className='btn btn-default hidden-sm'>Получить займ</a>
                  <a href='#' className='btn btn-default visible-sm'>Получить займ</a>
                </div>{/* col-sm-3 */}
              </div>{/* row */}
              <div className='visible-xs'>
                <div className='text-center'>
                  <a href='#' className='btn btn-default'>Получить Деньги</a>
                </div>
              </div>{/* visible-xs */}
            </div>{/* container */}
          </div>{/* green-container */}
          <div className='why-work'>
            <div className='container'>
              <div className='text-center'>
                <h2 className='mb-80px' style={{marginBottom: '45px'}}>Как это работает?</h2>
              </div>{/* text-center */}
              <div className='row'>
                <div className='col-sm-12 col-md-4'>
                  <div className='single-why text-center'>
                    <div className='sw-image mb-30px'>
                      <div className='sw-number'>1</div>
                      <img src='images/w1.png' alt />
                    </div>{/* sw-image */}
                    <div className='sw-title'>Заполните анкету</div>
                    <div className='arrow-one'><img src='images/arrow1.png' alt /></div>
                  </div>{/* single-why */}
                </div>{/* col-sm-4 */}
                <div className='col-sm-12 col-md-4'>
                  <div className='single-why text-center'>
                    <div className='sw-image mb-30px'>
                      <div className='sw-number'>2</div>
                      <img src='images/w2.png' alt />
                    </div>{/* sw-image */}
                    <div className='sw-title'>Заключение договора</div>
                    <div className='arrow-two'><img src='images/arrow2.png' alt /></div>
                  </div>{/* single-why */}
                </div>{/* col-sm-4 */}
                <div className='col-sm-12 col-md-4'>
                  <div className='single-why text-center'>
                    <div className='sw-image mb-30px'>
                      <div className='sw-number'>3</div>
                      <img src='images/w3.png' alt />
                    </div>{/* sw-image */}
                    <div className='sw-title'>Получите деньги</div>
                  </div>{/* single-why */}
                </div>{/* col-sm-4 */}
              </div>{/* row */}
            </div>{/* container */}
          </div>{/* why-work */}
          <div className='testimonials-container'>
            <div className='container'>
              <div className='text-center'>
                <h2 className='mb-80px' style={{marginBottom: '45px'}}>Ваши отзывы</h2>
              </div>{/* text-center */}
              <div className='testimonials-list'>
                <div className='row hidden-sm hidden-xs'>
                  <div className='col-sm-6'>
                    <div className='testimonial-block left-testi mb-30px'>
                      <div className='testimonial-text mb-30px'>
                        Очень перспективная и удобная организация, советую тем кто любит брать деньги с умом, после успешных погашенных вовремя займов компания идёт на уступки даже с плохой кредитной историей и проценты радуют в отличии от других МФО. Приятно с Вами работать!
                      </div>{/* testimonial-text */}
                      <div className='testi-user'>
                        <div className='testi-img'>
                          <img src='images/user.png' alt />
                        </div>{/* testi-img */}
                        <div className='testi-name'>
                          Александр, Тамбов
                        </div>{/* testi-name */}
                      </div>{/* testi-user */}
                    </div>{/* testimonial-block */}
                    <div className='testimonial-block right-testi mb-30px'>
                      <div className='testimonial-text mb-30px'>
                        Срочно понадобились деньги, все отказали, хоть и написано, что 100% без отказа. Moneyman сначала одобрил, позвонили, сказали, что одобрено, ждите перевод, денег так и не было. Здесь оперативно все и быстро без обмана. Огромное спасибо.
                      </div>{/* testimonial-text */}
                      <div className='testi-user'>
                        <div className='testi-name'>
                          Александр, Тамбов
                        </div>{/* testi-name */}
                        <div className='testi-img'>
                          <img src='images/user.png' alt />
                        </div>{/* testi-img */}
                      </div>{/* testi-user */}
                    </div>{/* testimonial-block */}
                  </div>{/* col-sm-8 */}
                  <div className='col-sm-offset-1 col-sm-4'>
                    <div className='testimonial-block left-testi mb-50px'>
                      <div className='testimonial-text mb-30px'>
                        Очень понятный сервис, простое и быстрое оформление, порадовала скорость перевода денег на карту.
                      </div>{/* testimonial-text */}
                      <div className='testi-user'>
                        <div className='testi-img'>
                          <img src='images/user2.png' alt />
                        </div>{/* testi-img */}
                        <div className='testi-name'>
                          Груздев Алексей Николаевич, Гидростроитель
                        </div>{/* testi-name */}
                      </div>{/* testi-user */}
                    </div>{/* testimonial-block */}
                    <div className='text-center tc'>
                      <a href='#popup-testi' className='btn btn-default mb-40px fancy'>Оставить отзыв</a>
                    </div>{/* text-center */}
                    <div className='text-center tc'>
                      <a href='#' className='all-testi'>Читать все отзывы</a>
                    </div>{/* text-center */}
                  </div>{/* col-sm-4 */}
                </div>{/* row */}
                <div className='visible-xs'>
                  <div className='testi-slider'>
                    <div className='items'>
                      <div className='testimonial-block left-testi mb-30px'>
                        <div className='testimonial-text mb-30px'>
                          Срочно понадобились деньги, все отказали, хоть и написано, что 100% без отказа. Moneyman сначала одобрил, позвонили, сказали, что одобрено, ждите перевод, денег так и не было. Здесь оперативно все и быстро без обмана. Огромное спасибо.
                        </div>{/* testimonial-text */}
                        <div className='testi-user'>
                          <div className='testi-img'>
                            <img src='images/user.png' alt />
                          </div>{/* testi-img */}
                          <div className='testi-name'>
                            Александр, Тамбов
                          </div>{/* testi-name */}
                        </div>{/* testi-user */}
                      </div>{/* testimonial-block */}
                    </div>{/* items */}
                    <div className='items'>
                      <div className='testimonial-block left-testi mb-30px'>
                        <div className='testimonial-text mb-30px'>
                          Срочно понадобились деньги, все отказали, хоть и написано, что 100% без отказа. Moneyman сначала одобрил, позвонили, сказали, что одобрено, ждите перевод, денег так и не было. Здесь оперативно все и быстро без обмана. Огромное спасибо.
                        </div>{/* testimonial-text */}
                        <div className='testi-user'>
                          <div className='testi-img'>
                            <img src='images/user.png' alt />
                          </div>{/* testi-img */}
                          <div className='testi-name'>
                            Александр, Тамбов
                          </div>{/* testi-name */}
                        </div>{/* testi-user */}
                      </div>{/* testimonial-block */}
                    </div>{/* items */}
                    <div className='items'>
                      <div className='testimonial-block left-testi mb-30px'>
                        <div className='testimonial-text mb-30px'>
                          Срочно понадобились деньги, все отказали, хоть и написано, что 100% без отказа. Moneyman сначала одобрил, позвонили, сказали, что одобрено, ждите перевод, денег так и не было. Здесь оперативно все и быстро без обмана. Огромное спасибо.
                        </div>{/* testimonial-text */}
                        <div className='testi-user'>
                          <div className='testi-img'>
                            <img src='images/user.png' alt />
                          </div>{/* testi-img */}
                          <div className='testi-name'>
                            Александр, Тамбов
                          </div>{/* testi-name */}
                        </div>{/* testi-user */}
                      </div>{/* testimonial-block */}
                    </div>{/* items */}
                  </div>{/* testi-slider */}
                  <div className='text-center tc'>
                    <a href='#popup-testi' className='btn btn-default mb-40px fancy'>Оставить отзыв</a>
                  </div>{/* text-center */}
                  <div className='text-center tc'>
                    <a href='#' className='all-testi'>Читать все отзывы</a>
                  </div>{/* text-center */}
                </div>{/* visible-xs */}
              </div>{/* testimonials-list */}
            </div>{/* container */}
          </div>{/* testimonials-container */}
          <div className='preimushestva-container'>
            <div className='container'>
              <div className='text-center'>
                <h2 className='mb-80px'>Наши преимущества</h2>
              </div>{/* text-center */}
              <div className='prem-list'>
                <div className='row hidden-xs'>
                  <div className='col-md-4 col-sm-6'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p1.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Круглосуточная работа онлайн.
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* col-sm-4 */}
                  <div className='col-md-4 col-sm-6'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p2.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Срочный перевод денег
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* col-sm-4 */}
                  <div className='col-md-4 col-sm-6'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p3.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        По одному паспорту.
                        Без залога и поручителя
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* col-sm-4 */}
                  <div className='col-md-4 col-sm-6'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p4.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Акции и специальные предложения
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* col-sm-4 */}
                  <div className='col-md-4 col-sm-6'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p5.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Перевод на банковскую карту или счет
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* col-sm-4 */}
                  <div className='col-md-4 col-sm-6'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p6.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Удобные способы погашения займа
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* col-sm-4 */}
                  <div className='col-md-4 col-sm-6'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p7.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Продление и досрочное погашение займа
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* col-sm-4 */}
                  <div className='col-md-4 col-sm-6'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p8.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Снижение ставки уже со второго обращения
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* col-sm-4 */}
                  <div className='col-md-4 col-sm-6'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p9.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Привилегии для постоянных клиентов
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* col-sm-4 */}
                </div>{/* row */}
                <div className='preim-slider visible-xs'>
                  <div className='items'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p1.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Круглосуточная работа онлайн.
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p2.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Срочный перевод денег
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p3.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        По одному паспорту.
                        Без залога и поручителя
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* items */}
                  <div className='items'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p4.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Акции и специальные предложения
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p5.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Перевод на банковскую карту или счет
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p6.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Удобные способы погашения займа
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* items */}
                  <div className='items'>
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p7.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Продление и досрочное погашение займа
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p8.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Снижение ставки уже со второго обращения
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                    <div className='preim-block mb-50px'>
                      <div className='preim-image'>
                        <img src='images/p9.png' alt />
                      </div>{/* preim-image */}
                      <div className='preim-text'>
                        Привилегии для постоянных клиентов
                      </div>{/* preim-text */}
                    </div>{/* preim-block */}
                  </div>{/* items */}
                </div>{/* -preim-slider */}
              </div>{/* preim-list */}
            </div>{/* container */}
          </div>{/* preimushestva-container */}
          <div className='procent-container'>
            <div className='container'>
              <div className='text-center'>
                <h2 className='mb-30px'>Ноль процентов по займу!</h2>
                <div className='pr-text mb-30px'>
                  Только с 1 по 12 сентября
                </div>{/* pr-text */}
                <VivaButton {...defaultButtonProps} value={'Промо-код'} />
              </div>{/* text-center */}
            </div>{/* container */}
          </div>{/* procent-container */}
        </section>{/* content */}
        <div className='container'>
          {/* <div className='col-xs-offset-2 col-xs-8'> */}
          <VivaFooter />
          {/* </div> */}
        </div>
        <div id='popup-testi' className='popup' />
      </div>
    )
  }
}
