import React, { Component, PropTypes } from 'react'
import { IssuePageBankCardData } from './IssuePageBankCardData'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import { defaultButtonFluidProps } from './../ui-kit/VivaButton'
import { VivaCheckbox } from './../ui-kit/VivaCheckbox'
import { VivaButtonGroup } from '../ui-kit/VivaButtonGroup'
import {  } from './../services/__validator'

const { any } = PropTypes

export class IssuePageFormStepFour extends Component {
  render () {
    const props = this.props
    const credit = this.props.credit
    const card = <span><i style={{fontSize: '18px', marginTop: '-20px'}} className='fa fa-credit-card' aria-hidden='true' />&nbsp;БАНКОВСКАЯ КАРТА</span>
    const bank = <span><i style={{fontSize: '18px', marginTop: '-20px'}} className='fa fa-university' aria-hidden='true' />&nbsp;БАНКОВСКИЙ СЧЕТ</span>
    const amountType = [{view: card, model: '1'}, {view: bank, model: '2'}]

    return (
      <section>
        <div className='form-tabs'>
          <div className='container'>
            <div className='row'>
              <div className='col-sm-offset-2 col-sm-8'>
                <div className='row'>
                  <div className='col-sm-8'>
                    <div className='f-inp'>
                      <label htmlFor>Получить займ на</label>
                      <VivaButtonGroup activeCondition={credit['requisite[type]'].value} value={credit['requisite[type]'].value} name='requisite[type]' valueSet={amountType} {...defaultButtonFluidProps} handleParentComponentStateChange={props.handleIssueFormChange} />
                    </div>
                  </div>
                </div>
              </div>{ /* col-sm-8 */ }
            </div>{ /* row */ }
          </div>{ /* container */ }
          { /*  Tab panes  */ }
          <div className='tab-content'>
            {credit['requisite[type]'].value === '1' && (
            <IssuePageBankCardData {...props} />
            )}
            {credit['requisite[type]'].value === '2' && (
            <div className='tab-pane active' id='profile'>
              <div className='container'>
                <div className='row'>
                  <div className='col-sm-offset-2 col-sm-8'>
                    <div className='f-inp'>
                      <label htmlFor>БИК банка ?</label>
                      <VivaInput {...defaultInputFluidProps} name='requisite[card-number]' error={!credit['requisite[cardNumber]'].isValid} value={credit['requisite[cardNumber]'].value} handleParentComponentStateChange={props.handleIssueFormChange} />
                    </div>{ /* f-inp */ }
                    <div className='f-inp'>
                      <label htmlFor>Счет банка ?</label>
                      <VivaInput {...defaultInputFluidProps} />
                    </div>{ /* f-inp */ }
                  </div>{ /* col-sm-8 */ }
                </div>{ /* row */ }
              </div>{ /* container */ }
            </div>
            )}
          </div>
        </div>{ /* form-tabs */ }
      </section>
    )
  }
}

IssuePageFormStepFour.propTypes = {
  person: any,
  hasPhone: any,
  handleIssueFormChange: any
}
