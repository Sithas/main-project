import React, { Component, PropTypes } from 'react'
import { VivaDropdown, defaultDropdownFluidProps } from './../ui-kit/VivaDropdown'

const { any } = PropTypes

export class IssuePageFormStepThree extends Component {
  render () {
    const props = this.props
    const person = this.props.person
    const credit = this.props.credit
    return (
      <section>
        <div className='container'>
          <div className='row'>
            <div className='col-sm-offset-2 col-sm-8'>
              <div className='form-title mb-imp'>Уточняющие данные</div>
              <div className='row'>
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <label htmlFor>доход в месяц</label>
                    <VivaDropdown {...defaultDropdownFluidProps}
                      name='income'
                      handleParentComponentStateChange={props.handleIssueFormChange}
                      value={person.income.value}
                      options={props.dictionary.income}
                      placeholder={'Выберите из списка'} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <label htmlFor>Ежемесячные выплаты по кредитам</label>
                    <VivaDropdown {...defaultDropdownFluidProps}
                      name='active-credits'
                      handleParentComponentStateChange={props.handleIssueFormChange}
                      value={person.activeCredits.value}
                      options={props.dictionary.activeCredits}
                      placeholder={'Выберите из списка'} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
              </div>{ /* row */ }
              <div className='row mb-40px'>
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <label htmlFor>Семейное положение</label>
                    <VivaDropdown {...defaultDropdownFluidProps}
                      name='marital-status'
                      handleParentComponentStateChange={props.handleIssueFormChange}
                      value={person.maritalStatus.value}
                      options={props.dictionary.maritalStatus}
                      placeholder={'Выберите из списка'} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <label htmlFor>Образование</label>
                    <VivaDropdown {...defaultDropdownFluidProps}
                      name='education'
                      handleParentComponentStateChange={props.handleIssueFormChange}
                      value={person.education.value}
                      options={props.dictionary.education}
                      placeholder={'Выберите из списка'} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-6 */ }
              </div>{ /* row */ }
              <div className='row mb-20px'>
                <div className='col-sm-6'>
                  <div className='f-inp'>
                    <label htmlFor>Цель кредита</label>
                    <VivaDropdown {...defaultDropdownFluidProps}
                      name='custom[credit-purpose]'
                      handleParentComponentStateChange={props.handleIssueFormChange}
                      value={credit['custom[creditPurpose]'].value}
                      options={props.dictionary.creditPurpose}
                      placeholder={'Выберите из списка'} />
                  </div>{ /* f-inp */ }
                </div>{ /* col-sm-12 */ }
              </div>{ /* row */ }
            </div>{ /* col-sm-8 */ }
          </div>{ /* row */ }
        </div>{ /* container */ }
      </section>
    )
  }
}

IssuePageFormStepThree.propTypes = {
  person: any,
  handleIssueFormChange: any
}
