import React, {Component} from 'react'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import {VivaButton} from './../ui-kit/VivaButton'
import {VivaHeader} from './../ui-kit/VivaHeader'
import { VivaSlider } from './../ui-kit/VivaSlider'
import { VivaDropdown, defaultAutocompleteFluidProps, defaultDropdownFluidProps } from './../ui-kit/VivaDropdown'

// workaround - props and styles for component stage
const style = {
  color: '#00b5ad',
  padding: '10px'
}

const style1 = {
  color: '#00b5ad',
  padding: '10px',
  background: '#ffffff'
}

const styleh2 = {
  color: '#2185d0'
}

let button1 = {
  value: 'its reusable Button',
  size: 'medium'
}
let button2 = {
  value: 'its reusable Button',
  loading: true,
  size: 'medium'
}
let button3 = {
  value: 'its reusable Button',
  disabled: true,
  size: 'medium'
}
let button4 = {
  value: 'its reusable Button',
  disabled: true,
  loading: true,
  size: 'medium'
}
let button5 = {
  value: 'its reusable Button',
  size: 'medium'
}
let input1 = {
  value: 'its reusable input',
  placeholder: 'custom placeholder...'
}

export class ComponentsSet extends Component {
  render () {
    // this stage is just only for view ui kit
    return (
      <div style={{
        backgroundImage: 'url(./BusinessTheme.png)',
        backgroundRepeat: 'no-repeat',
        backgroundAttachment: 'fixed'
      }}>
        <h1>Заголовок</h1>
        <div>
          <VivaHeader theme='invertedTheme' />
        </div>
        <h1 style={style}>Кастомные кнопки</h1>
        <div style={style}>
          <VivaButton color='teal' {...button1} />
          <VivaButton color='teal' {...button2} />
          <VivaButton color='teal' {...button3} />
          <VivaButton color='teal' {...button4} />
        </div>
        <div style={style}>
          <VivaButton color='blue' {...button1} />
          <VivaButton color='blue' {...button2} />
          <VivaButton color='blue' {...button3} />
          <VivaButton color='blue' {...button4} />
        </div>
        <div style={style}>
          <VivaButton color='red' {...button1} />
          <VivaButton color='red' {...button2} />
          <VivaButton color='red' {...button3} />
          <VivaButton color='red' {...button4} />
        </div>
        <div style={style}>
          <VivaButton basic color='teal' {...button1} />
          <VivaButton basic color='teal' {...button2} />
          <VivaButton basic color='teal' {...button3} />
          <VivaButton basic color='teal' {...button4} />
        </div>
        <div style={style}>
          <VivaButton basic color='grey' {...button5} />
        </div>
        <h1 style={style}>Кастомные инпуты</h1>
        <div style={style1}>
          <h2 style={styleh2}>Обычный</h2>
          <VivaInput {...input1} />
          <VivaInput {...defaultInputFluidProps} placeholder={'Введите фамилию'} />
        </div>
        <div style={style1}>
          <h2 style={styleh2}>удлиненный с загрузкой</h2>
          <VivaInput {...input1} loading />
        </div>
        <div style={style1}>
          <h2 style={styleh2}>Закрытый</h2>
          <VivaInput {...input1} disabled />
        </div>
        <div style={style1}>
          <h2 style={styleh2}>Невалидный</h2>
          <VivaInput placeholder='my placeholder...' value='its error' error />
        </div>
        <div style={style}>
          <h2 style={styleh2}>Слайдер</h2>
          <VivaSlider />
        </div>
        <div style={style}>
          <h2 style={styleh2}>Дропдаун</h2>
          <VivaDropdown {...defaultDropdownFluidProps} />
        </div>
        <div style={style}>
          <h2 style={styleh2}>Автокомплит</h2>
          <VivaDropdown {...defaultAutocompleteFluidProps} />
        </div>
      </div>
    )
  }
}
