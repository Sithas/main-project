import React, { Component } from 'react'
import { VivaPaper } from '../ui-kit/VivaPaper'
import { Link } from 'react-router-dom'

export class DocumentWidget extends Component {
  render () {
    return (
      <VivaPaper>
        <div className='row'>
          <div className='col-md-12'>
            <h3>Документы</h3>
            <div className='row mb-10px'>
              <div className='col-sm-9'>
                Договор <span className='label label-default'>PDF</span>
              </div>
              <div className='col-sm-1'>
                <Link to='#'>СКАЧАТЬ</Link>
              </div>
            </div>
            <div className='row mb-10px'>
              <div className='col-sm-9'>
                Индивидуальные условия потребительского займа <span className='label label-default'>DOC</span>
              </div>
              <div className='col-sm-2'>
                <Link to='#'>СКАЧАТЬ</Link>
              </div>
            </div>
            <div className='row mb-10px'>
              <div className='col-sm-9'>
                Справка об отсутствии задолженности <span className='label label-default'>PDF</span>
              </div>
              <div className='col-sm-2'>
                <Link to='#'>СКАЧАТЬ</Link>
              </div>
            </div>
            <div className='row mb-10px'>
              <div className='col-sm-9'>
                Уведомление о передаче в коллекторское агенство <span className='label label-default'>PDF</span>
              </div>
              <div className='col-sm-2'>
                <Link to='#'>СКАЧАТЬ</Link>
              </div>
            </div>
            <div className='row mb-10px'>
              <div className='col-sm-9'>
                График платежей <span className='label label-default'>PDF</span>
              </div>
              <div className='col-sm-2'>
                <Link to='#'>СКАЧАТЬ</Link>
              </div>
            </div>
            <div className='row mb-10px'>
              <div className='col-sm-9'>
                История платежей <span className='label label-default'>XML</span>
              </div>
              <div className='col-sm-2'>
                <Link to='#'>СКАЧАТЬ</Link>
              </div>
            </div>
          </div>
        </div>
      </VivaPaper>
    )
  }
}
