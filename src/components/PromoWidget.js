import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { VivaPaper } from '../ui-kit/VivaPaper'
export class PromoWidget extends Component {
  render () {
    const colorWhite = {
      color: 'white'
    }

    const line = {
      border: '1px solid #FFFFFF'
    }
    return (
      <VivaPaper height='265px' backgroundImage='http://oilers.org.ua/wp-content/uploads/2017/03/nalogovaya-sistema-biznes-za-reformirovanie.jpg'>
        <div className='row'>
          <div className='col-xs-12'>
            <h1 style={colorWhite}>Ставка 0%</h1>
            <h3 style={colorWhite}>c 1 по 5 марта</h3>
          </div>
        </div>
        <div className='row'>
          <div className='col-xs-12 col-sm-4'>
            <hr style={line} />
            <p style={colorWhite}>Деньги срочно на карту или счет!
              По паспорту, без залога и поручителей!
              <Link to='#'>подробнее</Link></p>
          </div>
        </div>
      </VivaPaper>
    )
  }
}