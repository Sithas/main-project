import React, { Component } from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import { VivaHeader } from './../ui-kit/VivaHeader'
import { VivaPaper } from './../ui-kit/VivaPaper'
import { VivaFooter } from './../ui-kit/VivaFooter'
import { VivaPaperMessage, defaultErrorProps } from './../ui-kit/VivaPaperMessage'
import { VivaIconMaterialWarning } from './../ui-kit/VivaIcons'
import { VivaButton, tealButtonFluidProps } from './../ui-kit/VivaButton'
import { VivaTextArea, defaultTextAreaFluidProps } from './../ui-kit/VivaTextArea'
import './../styles/workarounds/span.scss'
import { DocumentWidget } from './DocumentWidget'
import { NewConditions } from './NewConditions'
import { RegistrationThanks } from './RegistrationThanks'
import { PromoWidget } from './PromoWidget'
import { PersonalMenu } from './PersonalMenu'
import { UserInfo } from './UserInfo'
import { UserFormData } from './UserFormData'
import { PersonalAreaRequisiteDataBank, PersonalAreaRequisiteDataCard, PersonalAreaRequisiteDataCredit } from './PersonalAreaRequisiteData'
import { HotLine } from './HotLine'
import { MessageArchive } from './MessageArchive'

export class PersonalArea extends Component {
  render () {
    const theme = 'invertedBackgroundTheme'
    const backgroundStyle = {
      background: '#fcf2fc'
    }
    return (
      <div style={backgroundStyle}>
        <VivaHeader theme={theme} />
        <div className='container-fluid'>
          <div className='col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10 col-xs-12'>
            <div className='row'>
              <div className='col-xs-12'>
                <div className='page-header'>
                  <h2>Личный кабинет</h2>
                  <h3>
                    <small>Здравствуйте, <b>Алексей</b>, добро пожаловать в ваш личный кабинет.</small>
                  </h3>
                </div>
              </div>
              <div className='col-xs-12'>
                <VivaPaperMessage {...defaultErrorProps} theme={'warningIconsTheme'} leftIcon={<VivaIconMaterialWarning theme='warningIconsTheme' constrainToInput />} >
                  Сообщение об ошибке
                </VivaPaperMessage>
              </div>
              <div className='col-xs-12'>
                <PersonalAreaRequisiteDataBank />
              </div>
              <div className='col-xs-12'>
                <PersonalAreaRequisiteDataCard />
              </div>
              <div className='col-xs-12'>
                <PersonalAreaRequisiteDataCredit />
              </div>
              <div className='col-xs-12'>
                <PromoWidget />
              </div>
            </div>
            <div className='row'>
              <div className='col-md-4 col-lg-3'>
                <UserInfo />
                <PersonalMenu />
              </div>
              <div className='col-md-8 col-lg-9'>
                <Switch>
                  <Route exact path={`/personal/qwerty`} component={() => <div>1234</div>} />
                </Switch>
                <div className='col-xs-12'>
                  <RegistrationThanks />
                </div>
                <div className='col-xs-12'>
                  <UserFormData />
                </div>
                <div className='col-xs-12'>
                  <HotLine />
                </div>
                <div className='col-xs-12'>
                  <MessageArchive />
                </div>
                <div className='col-xs-12'>
                  <VivaPaper>
                    <div>
                      <div className='row mb-15px'>
                        <div className='col-sm-8'>
                          <h3>Обращение 221123</h3><span className='type-request-close'>Закрыто</span>
                        </div>
                        <div className='col-sm-4'>
                          <p className='text-right'>1 января 2017</p>
                        </div>
                      </div>
                      <div className='row mb-15px'>
                        <div className='col-sm-12'>
                          <h4>Тема: не проходит платеж</h4>
                        </div>
                      </div>
                      <div className='row mb-10px'>
                        <div className='col-sm-12'>
                          <h4>Сообщение:</h4>
                        </div>
                      </div>
                      <div className='row mb-15px'>
                        <div className='col-sm-12'>
                          Обращаем Ваше внимание на то, что предоставление документов в электронной форме (обращений, запросов, уведомлений и иных) подразумевает наличие электронной подписи, с указанием соответствующего сертификата. В случае отсутствия электронной подписи просим направлять копии оригиналов документов по юридическому адресу, указанному в реквизитах. Информацию о реквизитах Вы можете найти на нашем сайте.
                        </div>
                      </div>
                    </div>
                    <div className='row mb-15px'>
                      <div className='col-sm-12'>
                        <h4>Документы:</h4>
                      </div>
                    </div>
                    <div className='row mb-15px'>
                      <div className='col-sm-12'>
                        <div className='row mb-10px'>
                          <div className='col-sm-9'>
                            График платежей <span className='label label-default'>PDF</span>
                          </div>
                          <div className='col-sm-1'>
                            <Link to='#'>СКАЧАТЬ</Link>
                          </div>
                        </div>
                        <div className='row mb-10px'>
                          <div className='col-sm-9'>
                            История платежей <span className='label label-default'>DOC</span>
                          </div>
                          <div className='col-sm-2'>
                            <Link to='#'>СКАЧАТЬ</Link>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='row mb-10px'>
                      <div className='col-sm-12'>
                        <h4>Ответ оператора:</h4>
                      </div>
                    </div>
                    <div className='row mb-15px'>
                      <div className='col-sm-12'>
                        Обращаем Ваше внимание на то, что предоставление документов в электронной форме (обращений, запросов, уведомлений и иных) подразумевает наличие электронной подписи, с указанием соответствующего сертификата. В случае отсутствия электронной подписи просим направлять копии оригиналов документов по юридическому адресу, указанному в реквизитах. Информацию о реквизитах Вы можете найти на нашем сайте.
                      </div>
                    </div>
                    <div className='row mb-15px'>
                      <div className='col-sm-6'>
                        <VivaButton {...tealButtonFluidProps} value='Новое обращение' />
                      </div>
                    </div>
                  </VivaPaper>
                </div>
                <div className='col-xs-12'>
                  <VivaPaper>
                    <div>
                      <div className='row mb-15px'>
                        <div className='col-sm-9'>
                          <h3>Обращение 221123</h3><span className='type-request-open'>Нужен ответ оператору</span>
                        </div>
                        <div className='col-sm-3'>
                          <p className='text-right'>1 января 2017</p>
                        </div>
                      </div>
                      <div className='row mb-15px'>
                        <div className='col-sm-12'>
                          <h4>Тема: не проходит платеж</h4>
                        </div>
                      </div>
                      <div className='row mb-10px'>
                        <div className='col-sm-12'>
                          <h4>Сообщение:</h4>
                        </div>
                      </div>
                      <div className='row mb-15px'>
                        <div className='col-sm-12'>
                          Обращаем Ваше внимание на то, что предоставление документов в электронной форме (обращений, запросов, уведомлений и иных) подразумевает наличие электронной подписи, с указанием соответствующего сертификата. В случае отсутствия электронной подписи просим направлять копии оригиналов документов по юридическому адресу, указанному в реквизитах. Информацию о реквизитах Вы можете найти на нашем сайте.
                        </div>
                      </div>
                    </div>
                    <div className='row mb-15px'>
                      <div className='col-sm-12'>
                        <h4>Документы:</h4>
                      </div>
                    </div>
                    <div className='row mb-15px'>
                      <div className='col-sm-12'>
                        <div className='row mb-10px'>
                          <div className='col-sm-9'>
                            График платежей <span className='label label-default'>PDF</span>
                          </div>
                          <div className='col-sm-1'>
                            <Link to='#'>СКАЧАТЬ</Link>
                          </div>
                        </div>
                        <div className='row mb-10px'>
                          <div className='col-sm-9'>
                            История платежей <span className='label label-default'>DOC</span>
                          </div>
                          <div className='col-sm-2'>
                            <Link to='#'>СКАЧАТЬ</Link>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='row mb-10px'>
                      <div className='col-sm-12'>
                        <h4>Ответ оператора:</h4>
                      </div>
                    </div>
                    <div className='row mb-15px'>
                      <div className='col-sm-12'>
                        Обращаем Ваше внимание на то, что предоставление документов в электронной форме (обращений, запросов, уведомлений и иных) подразумевает наличие электронной подписи, с указанием соответствующего сертификата. В случае отсутствия электронной подписи просим направлять копии оригиналов документов по юридическому адресу, указанному в реквизитах. Информацию о реквизитах Вы можете найти на нашем сайте.
                      </div>
                    </div>
                    <div className='row mb-10px'>
                      <div className='col-sm-12'>
                        <div className='f-inp'>
                          <label htmlFor>ваш ответ</label>
                          <VivaTextArea {...defaultTextAreaFluidProps} rows='3' placeholder='Текст сообщения' />
                        </div>
                      </div>
                    </div>
                    <div className='row mb-10px'>
                      <div className='col-sm-12'>
                        <div className='row mb-10px'>
                          <div className='col-sm-9'>
                            График платежей <span className='label label-default'>PDF</span>
                          </div>
                          <div className='col-sm-1'>
                            <Link to='#'>УДАЛИТЬ</Link>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className='row mb-15px'>
                      <div className='col-sm-6'>
                        <Link to='#'>ПРИКРЕПИТЬ ФАЙЛ</Link>
                      </div>
                    </div>
                    <div className='row mb-15px'>
                      <div className='col-sm-6'>
                        <VivaButton {...tealButtonFluidProps} value='Новое обращение' />
                      </div>
                    </div>
                  </VivaPaper>
                </div>
                <div className='col-xs-12'>
                  <DocumentWidget />
                </div>
                <div className='col-xs-12'>
                  <NewConditions />
                </div>
              </div>
            </div>
            <VivaFooter />
          </div>
        </div>
      </div>
    )
  }
}
