import React, { Component, PropTypes } from 'react'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import { VivaDropdown, defaultAutocompleteFluidProps } from './../ui-kit/VivaDropdown'

const { bool, any } = PropTypes

export class IssuePageAddressData extends Component {
  render () {
    const props = this.props
    const person = this.props.person

    const city = {
      getContent: (isFactNotEqualReg, key) => {
        const addressIndex = isFactNotEqualReg ? '2' : '1'
        return <div className='row' key={key}>
          <div className='col-sm-6'>
            <div className='f-inp'>
              <label htmlFor>Город</label>
              <VivaDropdown {...defaultAutocompleteFluidProps} placeholder={'Введите город'}
                name={`address[${addressIndex}][composite-address][city]`}
                handleParentComponentStateChange={props.handleIssueFormChange}
                value={person[`address[${addressIndex}][compositeAddress][city]`].value}
                options={props.citiesList} handleDrawContent={props.handleAutocompleteDrawCities} />
            </div>
          </div>
          <div className='col-sm-6'>
            <div className='f-inp'>
              <label htmlFor>индекс</label>
              <VivaDropdown {...defaultAutocompleteFluidProps} placeholder={'Введите индекс'}
                name={`address[${addressIndex}][composite-address][zip-code]`}
                handleParentComponentStateChange={props.handleIssueFormChange}
                value={person[`address[${addressIndex}][compositeAddress][zipCode]`].value}
                options={props.codesList} handleDrawContent={props.handleAutocompleteDrawCodes} />
            </div>
          </div>
        </div>
      }
    }

    const street = {
      getContent: (isFactNotEqualReg, key) => {
        const addressIndex = isFactNotEqualReg ? '2' : '1'
        return <div className='row' key={key}>
          <div className='col-sm-12'>
            <div className='f-inp'>
              <label htmlFor>Улица</label>
              <VivaInput {...defaultInputFluidProps} placeholder={'Введите город'}
                name={`address[${addressIndex}][composite-address][street]`}
                handleParentComponentStateChange={props.handleIssueFormChange}
                value={person[`address[${addressIndex}][compositeAddress][street]`].value} />
            </div>
          </div>
        </div>
      }
    }

    const homeNumber = {
      getContent: (isFactNotEqualReg, key) => {
        const addressIndex = isFactNotEqualReg ? '2' : '1'
        return <div className='row' key={key}>
          <div className='col-sm-3'>
            <div className='f-inp'>
              <label htmlFor>№ Дома</label>
              <VivaInput {...defaultInputFluidProps} name={`address[${addressIndex}][composite-address][house]`}
                handleParentComponentStateChange={props.handleIssueFormChange}
                value={person[`address[${addressIndex}][compositeAddress][house]`].value} placeholder={''} />
              <a onClick={props.handleRegistrationPrivateHome} className='pull-right right-link'
                style={{cursor: 'pointer'}}>Многоквартирный дом</a>
            </div>
          </div>
          <div className='col-sm-3'>
            <div className='f-inp'>
              <label htmlFor>квартира</label>
              <VivaInput {...defaultInputFluidProps} name={`address[${addressIndex}][composite-address][flat]`}
                handleParentComponentStateChange={props.handleIssueFormChange}
                value={person[`address[${addressIndex}][compositeAddress][flat]`].value} placeholder={''} />
              <a onClick={props.handleRegistrationPrivateHome} className='pull-right right-link'
                style={{cursor: 'pointer'}}>Частный дом</a>
            </div>
          </div>
        </div>
      }
    }

    const addressFields = [city, street, homeNumber]

    return (
      <div>
        <div className='form-one-block mb-60px'>
          <div className='form-title'>Адрес регистрации</div>
          <div>{addressFields.map((field, index) => (
            field.getContent(false, index)
            ))}
          </div>
        </div>
        {props.isFactNotEqualReg && (
          <div className='form-one-block mb-60px'>
            <div className='form-title'>Адрес фактического проживания</div>
              {addressFields.map((field, index) => (
                  field.getContent(props.isFactNotEqualReg, index)
              ))}
          </div>
        )}
        <div className='mb-50px'>
          <a onClick={props.handleFactAddress} style={{cursor: 'pointer'}}>Мой адрес проживания {!props.isFactNotEqualReg && <span>не</span>} совпадает с адресом регистрации</a>
        </div>{ /* mb-50px */ }
      </div>
    )
  }
}

IssuePageAddressData.propTypes = {
  person: any,
  handleIssueFormChange: any,
  registrationPrivateHome: bool,
  handleRegistrationPrivateHome: any,
  handleFactAddress: any,
  isFactNotEqualReg: any,
  factPrivateHome: any,
  handleFactPrivateHome: any
}
