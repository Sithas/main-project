import React, { Component, PropTypes } from 'react'
import { VivaCheckbox } from './../ui-kit/VivaCheckbox'
import { componentThemes } from './../ui-kit/colors/variables'
import moment from 'moment/src/moment'
import { __calculatePercentByDays } from './../services/__data'
import { __formatMoney } from './../services/__filter'

const { defaultTheme } = componentThemes
const { $primaryColor1, $textColor1 } = defaultTheme

export class IssuePageFormStepFive extends Component {
  render () {
    const {credit} = this.props
    const props = this.props
    const returnAmount = __calculatePercentByDays(Number(credit['amount'].value), credit['amountDays'].value, props.loanCondition.percent)
    const mainBlock = {
      border: `1px solid ${$primaryColor1}`,
      padding: '18px 25px'
    }
    const headerText = {
      fontWeight: '300',
      marginTop: '8px',
      fontSize: '18px',
      color: $primaryColor1
    }
    const contentText = {
      fontWeight: '300',
      marginTop: '4px',
      fontSize: '18px',
      color: $textColor1
    }
    const checkBoxMargin = {
      marginTop: '15px'
    }
    return (
      <section>
        <div className='container'>
          <div className='row'>
            <div style={mainBlock} className='col-sm-offset-2 col-sm-8'>
              <div className='form-result-block'>
                <div className='row'>
                  <div className='col-sm-4'>
                    <div className='text-left'>
                      <div style={headerText} className='form-label'>
                        Сумма займа:
                      </div>
                      <div style={contentText} className='form-r'>
                        {props.credit.amount.value}
                      </div>
                    </div>{ /* text-center */ }
                  </div>{ /* col-sm-4 */ }
                  <div className='col-sm-4'>
                    <div className='text-center'>
                      <div style={headerText} className='form-label'>
                        К возврату:
                      </div>
                      <div style={contentText} className='form-r'>
                        {__formatMoney(returnAmount)}
                      </div>
                    </div>{ /* text-center */ }
                  </div>{ /* col-sm-4 */ }
                  <div className='col-sm-4'>
                    <div className='text-center'>
                      <div style={headerText} className='form-label'>
                        Срок возврата
                      </div>
                      <div style={contentText} className='form-r'>
                        {moment(props.credit.returnDate.value).format('D MMMM YYYY')}
                      </div>
                    </div>{ /* text-center */ }
                  </div>{ /* col-sm-4 */ }
                </div>{ /* row */ }
              </div>{ /* form-rresult-block */ }
              <div style={checkBoxMargin} className='check-block'>
                <VivaCheckbox checked={props.person.personalAgreementConsent} name='personal-agreement-consent' handleParentComponentStateChange={props.handleIssueFormChange} />
                <label style={{verticalAlign: 'super', paddingLeft: '0', width: 'calc(100% - 80px)', display: 'inline'}} className='check'>
                  Я согласен с <a href='#'>общими условиями предоставления займа</a>
                </label>
              </div>
            </div>{ /* col-sm-8 */ }
          </div>{ /* row */ }
        </div>{ /* container */ }
      </section>
    )
  }
}
