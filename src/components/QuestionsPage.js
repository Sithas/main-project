import React, { Component } from 'react'
import { VivaHeader } from './../ui-kit/VivaHeader'
import { VivaButton, defaultButtonProps } from './../ui-kit/VivaButton'
import { VivaFooter } from '../ui-kit/VivaFooter'

export class QuestionsPage extends Component {
  render () {
    return (
      <div>
      <VivaHeader theme={'defaultTheme'} />
        <div style={{marginTop: '124px'}}>{ /* Костыль для корректного отображения хедера */ }</div>
        <section id='content' className='mb-70px'>
          <div className='questions-container'>
            <div className='container'>
              <div className='text-center'>
                <h2 className='mb-30px'>Вопросы и ответы</h2>
              </div>{ /* text-center */ }
              <div className='questions-list mb-20px;'>
                <div className='panel-group' id='accordion'>
                  <div className='panel panel-default'>
                    <div className='panel-heading'>
                      <div className='panel-title'>
                        <a data-toggle='collapse' data-parent='#accordion' href='#collapseOne'>
                          Как получить заём?
                          <span className='glyphicon glyphicon-remove pull-right' />
                        </a>
                      </div>
                    </div>
                    <div id='collapseOne' className='panel-collapse collapse in'>
                      <div className='panel-body'>
                        Существует 4 основных способа погашения займа. Оплатить заём можно банковской картой, банковским переводом, с помощью сервиса Элекснет (доступны опции погашения в платежных терминалах Элекснет или с помощью Кошелька Элекснет), через платежные терминалы Qiwi или Qiwi Кошелек, все необходимые реквизиты есть в личном кабинете. Обращаем Ваше внимание на то, что перевод банковским платежом может занять до 3 рабочих дней. Просим учитывать эту информацию при выборе данного способа оплаты в выходные и праздничные дни. Кроме того, до 80% комиссии за пользования займом Вы можете оплатить с помощью баллов, подробнее о том, как получить баллы можно узнать здесь.
                      </div>
                    </div>
                  </div>
                  <div className='panel panel-default'>
                    <div className='panel-heading'>
                      <div className='panel-title'>
                        <a data-toggle='collapse' className='collapsed' data-parent='#accordion' href='#collapseTwo'>
                          Как выплатить заём?
                          <span className='glyphicon glyphicon-remove pull-right' />
                        </a>
                      </div>
                    </div>
                    <div id='collapseTwo' className='panel-collapse collapse'>
                      <div className='panel-body'>
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                  <div className='panel panel-default'>
                    <div className='panel-heading'>
                      <div className='panel-title'>
                        <a data-toggle='collapse' className='collapsed' data-parent='#accordion' href='#collapseThree'>
                          Через сколько я получу решение?
                          <span className='glyphicon glyphicon-remove pull-right' />
                        </a>
                      </div>
                    </div>
                    <div id='collapseThree' className='panel-collapse collapse'>
                      <div className='panel-body'>
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                  <div className='panel panel-default'>
                    <div className='panel-heading'>
                      <div className='panel-title'>
                        <a data-toggle='collapse' className='collapsed' data-parent='#accordion' href='#collapse4'>
                          Как я узнаю результат?
                          <span className='glyphicon glyphicon-remove pull-right' />
                        </a>
                      </div>
                    </div>
                    <div id='collapse4' className='panel-collapse collapse'>
                      <div className='panel-body'>
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                </div>
              </div>{ /* questions-list */ }
              <div className='text-center'>
                <VivaButton {...defaultButtonProps} value={'Задать свой вопрос'} />
              </div>{ /* text-center */ }
            </div>{ /* container */ }
          </div>{ /* questions-container */ }
          <div className='procent-container'>
            <div className='container'>
              <div className='text-center'>
                <h2 className='mb-30px'>Ноль процентов по займу!</h2>
                <div className='pr-text mb-30px'>
                  Только с 1 по 12 сентября
                </div>{ /* pr-text */ }
                <VivaButton {...defaultButtonProps} value={'Получить промокод'} />
              </div>{ /* text-center */ }
            </div>{ /* container */ }
          </div>{ /* procent-container */ }
        </section>{ /* content */ }
        <div className='col-lg-offset-1 col-lg-10'>
        <VivaFooter />
        </div>
      </div>
    )
  }
}
