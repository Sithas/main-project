import React, { Component, PropTypes } from 'react'
import { IssuePageAddressData } from './IssuePageAddressData'
import { VivaInput, defaultInputFluidProps } from './../ui-kit/VivaInput'
import { VivaCalendar } from './../ui-kit/VivaCalendar'
import { VivaFileUploader } from './../ui-kit/VivaFileUploader'
import { VivaImageViewer } from './../ui-kit/VivaImageViewer'
import { defaultButtonFluidProps } from './../ui-kit/VivaButton'
import { componentThemes } from './../ui-kit/colors/variables'

const { bool, any } = PropTypes

export class IssuePageFormStepTwo extends Component {
  render () {
    const props = this.props
    const person = this.props.person
    const { $primaryColor1, $textColor1 } = componentThemes.defaultTheme
    const textStyle = {
      color: $textColor1,
      marginTop: '15px',
      marginBottom: '4px',
      textAlign: 'center'
    }
    const textStyleSmall = {
      color: $primaryColor1,
      marginTop: '15px',
      marginBottom: '4px',
      textAlign: 'center'
    }
    return (
      <section>
        <div className='container'>
          <div className='row'>
            <div className='col-sm-offset-2 col-sm-8'>
              <div className='form-one-block mb-60px'>
                <div className='form-title'>Паспортные данные</div>
                <div className='row'>
                  <div className='col-sm-6'>
                    <div className='f-inp'>
                      <label htmlFor>Номер паспорта</label>
                      <VivaInput {...defaultInputFluidProps} name='identity-document[number]' handleParentComponentStateChange={props.handleIssueFormChange} value={person['identityDocument[number]'].value} placeholder={'Введите данные'} />
                    </div>{ /* f-inp */ }
                  </div>{ /* col-sm-6 */ }
                  <div className='col-sm-6'>
                    <div className='f-inp'>
                      <label htmlFor>Место рождения</label>
                      <VivaInput {...defaultInputFluidProps} name='birth-place' handleParentComponentStateChange={props.handleIssueFormChange} value={person.birthPlace.value} placeholder={'Введите данные'} />
                      <a className='pull-right right-link' style={{cursor: 'pointer'}}>Введите современное название населенного пункта</a>
                    </div>{ /* f-inp */ }
                  </div>{ /* col-sm-6 */ }
                  <div className='col-sm-6'>
                    <div className='f-inp'>
                      <label htmlFor>Дата окончания</label>
                      <VivaCalendar name='identity-document[expiration-date]' value={person['identityDocument[expirationDate]'].value} {...defaultButtonFluidProps} handleParentComponentStateChange={props.handleIssueFormChange} valueSet='simpledate' />
                    </div>{ /* f-inp */ }
                  </div>{ /* col-sm-6 */ }
                </div>{ /* row */ }
                <div className='row mb-40px'>
                  <div className='col-sm-12'>
                    <h4 style={textStyle}>Загрузите фотографию карты</h4>
                  </div>
                  <div className='col-sm-6'>
                    <h5 style={textStyleSmall}>С лицевой стороны</h5>
                    <VivaFileUploader name='identity-document-image-front' value={person.identityDocumentImageFront.value} handleParentComponentStateChange={props.handleIssueFormChange} />
                    <VivaImageViewer src={person.identityDocumentImageFront.value ? person.identityDocumentImageFront.value : './images/image-text.png'} />
                  </div>
                  <div className='col-sm-6'>
                    <h5 style={textStyleSmall}>С обратной стороны</h5>
                    <VivaFileUploader name='identity-document-image-back' value={person.identityDocumentImageBack.value} handleParentComponentStateChange={props.handleIssueFormChange} />
                    <VivaImageViewer src={person.identityDocumentImageBack.value ? person.identityDocumentImageBack.value : './images/image-text.png'} />
                  </div>
                </div>
                <IssuePageAddressData {...props} />
              </div>{ /* form-one-block */ }
            </div>{ /* col-sm-8 */ }
          </div>{ /* row */ }
        </div>{ /* container */ }
      </section>
    )
  }
}

IssuePageFormStepTwo.propTypes = {
  person: any,
  handleIssueFormChange: any,
  registrationPrivateHome: bool,
  handleRegistrationPrivateHome: any,
  handleFactAddress: any,
  isFactNotEqualReg: any,
  factPrivateHome: any,
  handleFactPrivateHome: any
}
