export const issuePageInvestModel = {
  secondNameInvest: {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  firstNameInvest: {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  identificationNumberInvest: {
    value: '',
    isValid: true,
    type: 'number',
    isRequired: true
  },
  identifyIdInvest: {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  sexIdInvest: {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  birthdayInvest: {
    value: '',
    isValid: true,
    type: 'date-form',
    isRequired: true
  },
  'contact[1][value]Invest': {
    value: '',
    isValid: true,
    type: 'phone',
    isRequired: true
  },
  'contact[2][value]Invest': {
    value: '',
    isValid: true,
    type: 'email',
    isRequired: true
  },
  'address[1][compositeAddress][house]Invest': {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  'address[2][compositeAddress][house]Invest': {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  'address[1][compositeAddress][city]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[1][compositeAddress][zipCode]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[1][compositeAddress][street]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[1][compositeAddress][block]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[1][compositeAddress][building]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[1][compositeAddress][flat]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][city]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][zipCode]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][street]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][block]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][building]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][flat]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  birthPlaceInvest: {
    value: '',
    isValid: true,
    type: 'text'
  },
  'identityDocument[typeId]Invest': {
    value: 1,
    isValid: true,
    type: 'text'
  },
  'identityDocument[number]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'identityDocument[expirationDate]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  incomeInvest: {
    value: '',
    isValid: true,
    type: 'text'
  },
  activeCreditsInvest: {
    value: '',
    isValid: true,
    type: 'text'
  },
  maritalStatusInvest: {
    value: '',
    isValid: true,
    type: 'text'
  },
  educationInvest: {
    value: '',
    isValid: true,
    type: 'text'
  },
  identityDocumentImageFrontInvest: {
    value: '',
    isValid: true,
    type: 'text'
  },
  identityDocumentImageBackInvest: {
    value: '',
    isValid: true,
    type: 'text'
  },
  personalAgreementConsentInvest: {
    value: '',
    isValid: true,
    type: 'checkbox'
  },
  personalDataConsentInvest: {
    value: '',
    isValid: true,
    type: 'checkbox'
  },
  receivingAdConsentInvest: {
    value: '',
    isValid: true,
    type: 'checkbox'
  },
  creditHistoryRequestConsentInvest: {
    value: '',
    isValid: true,
    type: 'checkbox'
  },
  emailAbsenceInvest: {
    value: '2',
    isValid: true,
    type: 'checkbox'
  }
}

export const issuePageInvestCreditModel = {
  amountInvest: {
    value: '10000',
    isValid: true,
    type: 'slider-input',
    isRequired: true
  },
  amountDaysInvest: {
    value: '23',
    isValid: true,
    type: 'slider-input',
    isRequired: true
  },
  returnDateInvest: {
    value: '',
    isValid: true,
    type: 'slider-input',
    isRequired: true
  },
  'custom[creditPurpose]Invest': {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  'requisite[type]Invest': {
    value: '1',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  'requisite[cardNumber]Invest': {
    value: '',
    isValid: true,
    type: 'luna-card',
    isRequired: true
  },
  'requisite[holderNameSurname]Invest': {
    value: '',
    isValid: true,
    type: 'upperCaseText',
    isRequired: true
  },
  'requisite[expirationDate]Invest': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'custom[loanWay]Invest': {
    value: '1',
    isValid: true,
    type: 'text'
  }
}
