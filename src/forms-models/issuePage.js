export const issuePagePersonModel = {
  secondName: {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  firstName: {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  identificationNumber: {
    value: '',
    isValid: true,
    type: 'number',
    isRequired: true
  },
  identifyId: {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  sexId: {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  birthday: {
    value: '',
    isValid: true,
    type: 'date-form',
    isRequired: true
  },
  'contact[1][value]': {
    value: '',
    isValid: true,
    type: 'phone',
    isRequired: true
  },
  'contact[2][value]': {
    value: '',
    isValid: true,
    type: 'email',
    isRequired: true
  },
  'address[1][compositeAddress][house]': {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  'address[2][compositeAddress][house]': {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  'address[1][compositeAddress][city]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[1][compositeAddress][zipCode]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[1][compositeAddress][street]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[1][compositeAddress][block]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[1][compositeAddress][building]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[1][compositeAddress][flat]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][city]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][zipCode]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][street]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][block]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][building]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'address[2][compositeAddress][flat]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  birthPlace: {
    value: '',
    isValid: true,
    type: 'text'
  },
  'identityDocument[typeId]': {
    value: 1,
    isValid: true,
    type: 'text'
  },
  'identityDocument[number]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'identityDocument[expirationDate]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  income: {
    value: '',
    isValid: true,
    type: 'text'
  },
  activeCredits: {
    value: '',
    isValid: true,
    type: 'text'
  },
  maritalStatus: {
    value: '',
    isValid: true,
    type: 'text'
  },
  education: {
    value: '',
    isValid: true,
    type: 'text'
  },
  identityDocumentImageFront: {
    value: '',
    isValid: true,
    type: 'text'
  },
  identityDocumentImageBack: {
    value: '',
    isValid: true,
    type: 'text'
  },
  personalAgreementConsent: {
    value: '',
    isValid: true,
    type: 'checkbox'
  },
  personalDataConsent: {
    value: '',
    isValid: true,
    type: 'checkbox'
  },
  receivingAdConsent: {
    value: '',
    isValid: true,
    type: 'checkbox'
  },
  creditHistoryRequestConsent: {
    value: '',
    isValid: true,
    type: 'checkbox'
  },
  emailAbsence: {
    value: '2',
    isValid: true,
    type: 'checkbox'
  }
}

export const issuePageCreditModel = {
  amount: {
    value: '10000',
    isValid: true,
    type: 'slider-input',
    isRequired: true
  },
  amountDays: {
    value: '23',
    isValid: true,
    type: 'slider-input',
    isRequired: true
  },
  returnDate: {
    value: '',
    isValid: true,
    type: 'slider-input',
    isRequired: true
  },
  'custom[creditPurpose]': {
    value: '',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  'requisite[type]': {
    value: '1',
    isValid: true,
    type: 'text',
    isRequired: true
  },
  'requisite[cardNumber]': {
    value: '',
    isValid: true,
    type: 'luna-card',
    isRequired: true
  },
  'requisite[holderNameSurname]': {
    value: '',
    isValid: true,
    type: 'upperCaseText',
    isRequired: true
  },
  'requisite[expirationDate]': {
    value: '',
    isValid: true,
    type: 'text'
  },
  'custom[loanWay]': {
    value: '1',
    isValid: true,
    type: 'text'
  }
}
