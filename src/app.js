import React, { Component } from 'react'
import {
  HashRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
import ReactDOM from 'react-dom'
import { MainPage } from './components/MainPage'
import { QuestionsPage } from './components/QuestionsPage'
import { IssuePage } from './components/IssuePage'
import { IssueInvestPage } from './components/IssueInvestPage'
import { ComponentsSet } from './components/ComponentsSet'
import { PersonalArea } from './components/PersonalArea'
import { LogInPage } from './components/LogInPage'

import './styles/styles.scss'
import './styles/workarounds/plaintext.scss'

class AppComponent extends Component {
  render () {
    return (
      <Router>
        <Switch>
          <Route exact path='/' component={MainPage} />
          <Route path='/questions' component={QuestionsPage} />
          <Route path='/issue' component={IssuePage} />
          <Route path='/invest' component={IssueInvestPage} />
          <Route path='/components' component={ComponentsSet} />
          <Route path='/personal' component={PersonalArea} />
          <Route path='/login' component={LogInPage} />
        </Switch>
      </Router>
    )
  }
}

ReactDOM.render(<AppComponent />, document.getElementById('app'))
